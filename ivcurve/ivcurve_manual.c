// Originally written by Paul Scott 2011/2012?
// Modified by Chris Jones
// 2012.08.15
// * removing accesses to 2600 board, because will not be using it for multiplexer, using serial port instead
// 2012.09.26
// * preparing to look for reason why ivcurve is using 100% of one cpu
//   - found it, it was a select call in comms.c
//<               //printf("selwait = {%ld s, %ld us}\n", (long)selwait.tv_sec, (long)selwait.tv_usec);
//<               //printf("Calling select()\n");
//<               tmpwait = selwait;
//<               if (select(FD_SETSIZE, &read_socks, NULL, NULL, &tmpwait) < 0) {
//---
//>               if (select(FD_SETSIZE, &read_socks, NULL, NULL, &selwait) < 0) {
// * removed commented-out code
//
//
// 2013.04.30 Changed 
// #define MODFN "/home/solaruser/s2600/conf/modules.cfg" // holds module id#, active/inactive, name
// to
// #define MODFN "/home/solaruser/weather/conf/modules.cfg" // holds module id#, active/inactive, name
//
// Allowed for 8 instead of 5 modules. (MUX_NCHANNELS)
//
// Added some error checking to load_cfg() to avoid bad file corrupting memory
//
// changed: static const char LOGFN[] = "/home/solaruser/s2600/log/ivcurve.log"; to
// static const char LOGFN[] = "/home/solaruser/weather/log/ivcurve.log";
//
// Changed mux access to use serial library cj_serial
//
// 2013.12.12
// Moved the following lines so that they get executed before each measurement of an IV curve,
//  even if (especially if) the measurement fails.
// 		mods[j].voc = 0.0;
//		mods[j].isc = 0.0;
//		mods[j].mpp = 0.0;
//
// 2014.02.20
// Created ivcurve_manual for manual IV curve tracing
//
//
//


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
//#include <pthread.h>
#include <time.h>
#include <math.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>

//#include <libxml/tree.h>

//#include "comms.h"
#include "cj_serial.h"
#include "ksm_control.h"
//#include "database.h"
//#include "logging.h"
#include "mux_control.h"

#define NPOINTS 101

#define NMODULES (MUX_NCHANNELS)
// Maximum number of modules on test (number of channels of MUX)
// Should match contents of file MODFN.

#define LEN_MOD_NAME 20 // Maximum length in characters for module name

//#define SOCKET_PATH "/tmp/ivcurve_socket"
//#define XMLFN "/var/www/data/ivcurve.xml" // holds: module ID#, name, active/inactive, VOC, ISC, MPP
// used by output_data()

//#define MODFN "/home/solaruser/weather/conf/modules.cfg" // holds module id#, active/inactive, name
// this is the file used by load_cfg() and save_cfg()
// The file is only accessed by this program, not by others.

//#define MEAS_INT 5 // Measurement interval (min)
//#define MEAS_INT 2 // Measurement interval (min)
// note that at 1 minute interval, putting results into database takes so long it misses measurements!

//#define MIN_ISC_FOR_IVCURVE (1e-6)
// Don't bother putting into the database any full IV curves with fabs(ISC) < this amount - but do still record Isc.

#define STR(s) #s // Stringify input
#define MSTR(s) STR(s) // Stringify macro result

//static const char LOGFN[] = "/home/solaruser/weather/log/ivcurve.log";

// Messaging errors - used by message_handler(), ?related to communication with web server?
//static const char *ERR_CMD = "err cmd";
//static const char *ACK = "ack";
//static const char *ERR_SET = "err set";


struct module {
	int active; // Indicates whether module should be traced
	char name[LEN_MOD_NAME+1]; // Name of module
        //double ivdata[2*PVPM_VALS];
	double vdata[NPOINTS];
	double idata[NPOINTS];
	double voc;
	double isc;
	double mpp;
	struct timeval meastime;
};

//static pthread_mutex_t mtx; // for message_handler() thread to not read/write setup while other threads are writing it ?

cj_serial_t ksm_portdata; // used to keep track of file handle for serial port, so that it can be closed.
const char ksm_devname[] = "/dev/ttyUSB-sourcemeter"; // device for sourcemeter
const long int ksm_baudrate = 57600; // Should match front-panel setting of sourcemeter
const int ksm_timeout_10ths = 10; // Timeout to wait for responses from Sourcemeter

cj_serial_t mux_portdata; // used to keep track of file handle for serial port, so that it can be closed.
const char mux_devname[] = "/dev/ttyUSB-rlymux";
const long int mux_baudrate = 9600; // Should match setting in firmware of mux
const int mux_timeout_10ths = 10; // Timeout to wait for responses from Mux (if any)

//static time_t nextmeas; // next measurement time
//static time_t thismeas; // measurement time

int measure_channel(int channelnum);

void shutdown();
void sigint_handler();
int write_results(int j, struct module *mod);

int main(int argc, char *argv[]){
	//char tbuf[21];
	//struct timespec slp_time;
	//struct tm lt;
	//	int intmins;
	//time_t wait_seconds;
	int err;
	int channelnum;
	//int j;
	
	// mark the ports as closed so that the sigint_handler() won't try to close them if they haven't been opened.
	cj_serial_mark_port_as_closed(&ksm_portdata);
	cj_serial_mark_port_as_closed(&mux_portdata);
	
	signal(SIGINT, sigint_handler);
	
	if(argc < 1) return 1;
	if(argc < 2){
		printf("usage: %s <serial device for sourcemeter> [<device for mux> <channel>]\n", argv[0]);
		return 1;
	}
	
	printf("Trying to open <%s> as serial device for Sourcemeter\n", argv[1]);
	
	// open and reset Sourcemeter
	err = ksm_open(&ksm_portdata, argv[1], ksm_baudrate, ksm_timeout_10ths);
	if (err) {
		fprintf(stderr, "ksm_open() failed.\n");
		return 1;
	}
	printf("debug : ksm is opened.\n");
	
	channelnum = 0;
	
	if(argc == 4){
		printf("Trying to open <%s> as serial device for mux, and will select channel %s\n", argv[2], argv[3]);
		
		if(sscanf(argv[3], "%d", &channelnum) != 1){
			fprintf(stderr, "couldn't get channel number.\n");
			return 1;
		}
		if((channelnum<0) || (channelnum >= MUX_NCHANNELS)){
			fprintf(stderr, "Error: bad channel number.\n");
			return 1;
		}

		err = mux_open(&mux_portdata, argv[2], mux_baudrate, mux_timeout_10ths);
		if (err){
			fprintf(stderr, "mux_open() failed.\n");
			return 1;
		}
		printf("debug : mux is opened\n");
	}

	if (ksm_reset(&ksm_portdata)){
		fprintf(stderr, "ksm_reset failed.\n");
		shutdown();
		return 1;
	}
	printf("debug : ksm is reset\n");

        if(ksm_remotesense_enable(&ksm_portdata, 1)){ // measure (and control) the voltage using the sense terminals
                fprintf(stderr, "ksm_remotesense_enable(... , 1) failed.");
                shutdown();
                return 1;
        }
        printf("debug : ksm remotesensing enabled");

	measure_channel(channelnum);
	shutdown();
	return 0;
}

int measure_channel(int channelnum){
	struct module mods[NMODULES];
	int j;
	int status, err;
	int mppindex;
	int k;

	for (j = 0; j < NMODULES; j++) {
		mods[j].active = 0;
	}
	mods[channelnum].active = 1;

	err = 0;
	for (j = 0; j < NMODULES; j++) {
		// Zero the variables so that if they fail to be set the readings shouldn't be too misleading
		mods[j].voc = 0.0;
		mods[j].isc = 0.0;
		mods[j].mpp = 0.0;
		for(k=0; k<NPOINTS; k++){
		  mods[j].vdata[k] = mods[j].idata[k] = 0.0;
		}

		if (mods[j].active) {
			printf("Measuring module %d\n", j);

			if(mux_portdata.f != -1){
				printf("Selecting MUX channel %d\n", j);
				mux_set(&mux_portdata, j); // Select module j for testing
			}

			// Start curve trace...
			status = ksm_measure_autorange_iv(&ksm_portdata, NPOINTS, &(mods[j].voc), &(mods[j].isc), mods[j].vdata, mods[j].idata, &(mods[j].meastime));

			if (status){
				fprintf(stderr, "ksm_measure_auorange_iv failed.");
				err = 1;
				break;
			}else{
				mppindex = calc_mpp_index(NPOINTS, mods[j].isc*mods[j].voc, mods[j].vdata, mods[j].idata);
				// assumed isc*voc has the right sign for power being generated
				mods[j].mpp = mods[j].vdata[mppindex]*mods[j].idata[mppindex];
			}
		}
	}

	if(mux_portdata.f != -1){
		printf("turning off MUX\n");
		mux_set(&mux_portdata, MUX_ALL_OFF);
	}

	// write out results
	for (j = 0; j < NMODULES; j++) {
		if (mods[j].active){
			status = write_results(j, &(mods[j]));
			if(status) err = 1;
		}
	}
	return err;
}

void shutdown(){
	// there is no harm in calling these, even if the ports are not open yet.
	ksm_close(&ksm_portdata);
	mux_close(&mux_portdata);
}

void sigint_handler(){
	shutdown();
	
	signal(SIGINT, SIG_DFL); // Reattach quiting handler for SIGINT
	kill(getpid(), SIGINT);
}

// Return 0 for success, nonzero for failure
int write_results(int j, struct module *mod){
	int k;
	FILE *fp;
	char *filename = "ivcurve.csv";
	 
	fp = fopen(filename, "w");
	if(!fp){
		fprintf(stderr, "couldn't open output file.\n");
		return 1;
	}

	fprintf(fp, ",,Voc,Isc,Pmax\n");
	fprintf(fp, ",,%f,%f,%f\n", (double)mod->voc, (double)mod->isc, (double)mod->mpp);
	fprintf(fp, "V,I\n");
	for(k=0; k<NPOINTS; k++){
		fprintf(fp,"%f,%f\n", (mod->vdata)[k], (mod->idata)[k]);
	}

	fclose(fp);
	return 0;
}

