// Originally written by Paul Scott 2011/2012?
// Modified by Chris Jones
// 2012.08.15
// * removing accesses to 2600 board, because will not be using it for multiplexer, using serial port instead
// 2012.09.26
// * preparing to look for reason why ivcurve is using 100% of one cpu
//   - found it, it was a select call in comms.c
//<               //printf("selwait = {%ld s, %ld us}\n", (long)selwait.tv_sec, (long)selwait.tv_usec);
//<               //printf("Calling select()\n");
//<               tmpwait = selwait;
//<               if (select(FD_SETSIZE, &read_socks, NULL, NULL, &tmpwait) < 0) {
//---
//>               if (select(FD_SETSIZE, &read_socks, NULL, NULL, &selwait) < 0) {
// * removed commented-out code
//
//
// 2013.04.30 Changed 
// #define MODFN "/home/solaruser/s2600/conf/modules.cfg" // holds module id#, active/inactive, name
// to
// #define MODFN "/home/solaruser/weather/conf/modules.cfg" // holds module id#, active/inactive, name
//
// Allowed for 8 instead of 5 modules. (MUX_NCHANNELS)
//
// Added some error checking to load_cfg() to avoid bad file corrupting memory
//
// changed: static const char LOGFN[] = "/home/solaruser/s2600/log/ivcurve.log"; to
// static const char LOGFN[] = "/home/solaruser/weather/log/ivcurve.log";
//
// Changed mux access to use serial library cj_serial
//
// 2013.12.12
// Moved the following lines so that they get executed before each measurement of an IV curve,
//  even if (especially if) the measurement fails.
// 		mods[j].voc = 0.0;
//		mods[j].isc = 0.0;
//		mods[j].mpp = 0.0;
//
// 2015.05.14
// Added code to put the Sourcemeter into 4-wire mode.
// Before this change, measurements were inaccurate if taken with long wires and high currents.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <math.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/stat.h>
#include <errno.h>

#include <libxml/tree.h>

#include "comms.h"
#include "cj_serial.h"
#include "ksm_control.h"
#include "database.h"
#include "logging.h"
#include "mux_control.h"


//#define ADD_RESULTS_TO_DATABASE

#define NPOINTS 101

#define NMODULES (MUX_NCHANNELS)
// Maximum number of modules on test (number of channels of MUX)
// Should match contents of file MODFN.

#define LEN_MOD_NAME 20 // Maximum length in characters for module name

#define SOCKET_PATH "/tmp/ivcurve_socket"
#define XMLFN "/var/www/data/ivcurve.xml" // holds: module ID#, name, active/inactive, VOC, ISC, MPP
// used by output_xml()

#define MODFN "/home/solaruser/weather/conf/modules.cfg" // holds module id#, active/inactive, name
// this is the file used by load_cfg() and save_cfg()
// The file is only accessed by this program, not by others.

#define DATAFN_ARCHIVEPATH "/var/www/data/ivcurve/"
#define DATAFN_PROCESSPATH "/home/solaruser/weather_data/to_process/ivcurve/"


#define MEAS_INT 1 // Measurement interval (min)
//#define MEAS_INT 5 // Measurement interval (min)


#define MIN_ISC_FOR_IVCURVE (1e-6)
// Don't bother putting into the database any full IV curves with fabs(ISC) < this amount - but do still record Isc.

#define STR(s) #s // Stringify input
#define MSTR(s) STR(s) // Stringify macro result

static const char LOGFN[] = "/home/solaruser/weather/log/ivcurve.log";

// Messaging errors - used by message_handler(), ?related to communication with web server?
static const char *ERR_CMD = "err cmd";
static const char *ACK = "ack";
static const char *ERR_SET = "err set";


struct module {
	int active; // Indicates whether module should be traced
	char name[LEN_MOD_NAME+1]; // Name of module
        //double ivdata[2*PVPM_VALS];
	double vdata[NPOINTS];
	double idata[NPOINTS];
	double voc;
	double isc;
	double vmp;
	double imp;
	double mpp;
	struct timeval meastime;
};

static pthread_mutex_t mtx; // for message_handler() thread to not read/write setup while other threads are writing it ?

static struct module mods[NMODULES];


cj_serial_t ksm_portdata; // used to keep track of file handle for serial port, so that it can be closed.
const char ksm_devname[] = "/dev/ttyUSB-sourcemeter"; // device for sourcemeter
const long int ksm_baudrate = 57600; // Should match front-panel setting of sourcemeter
const int ksm_timeout_10ths = 10; // Timeout to wait for responses from Sourcemeter

cj_serial_t mux_portdata; // used to keep track of file handle for serial port, so that it can be closed.
const char mux_devname[] = "/dev/ttyUSB-rlymux";
const long int mux_baudrate = 9600; // Should match setting in firmware of mux
const int mux_timeout_10ths = 10; // Timeout to wait for responses from Mux (if any)

static time_t nextmeas; // next measurement time
static time_t thismeas; // measurement time

static void main_loop();
static void measure_channels(void);
static void msg_handler(const char *msg, char *rply);
static void shutdown();
static void sigint_handler();
#ifdef ADD_RESULTS_TO_DATABASE
static int database_module(const int j, const time_t time, const struct module mod);
#endif
static void output_xml();
static int load_cfg();
static int save_cfg();


#define LEN_TIMESTRING 100
#define LEN_FILENAME (LEN_TIMESTRING + 500)
#define FILENAME_TIME_RESOLUTION_YEAR 1
#define FILENAME_TIME_RESOLUTION_MONTH 2
#define FILENAME_TIME_RESOLUTION_DAY 3
#define FILENAME_TIME_RESOLUTION_HOUR 4
#define FILENAME_TIME_RESOLUTION_MINUTE 5
#define FILENAME_TIME_RESOLUTION_SECOND 6
static void create_filename(char *filename, char *basepath, time_t t1, int time_resolution){
        struct tm t2;
        char timestring[LEN_TIMESTRING];
	
        localtime_r(&t1, &t2);
	
        switch(time_resolution){
        case FILENAME_TIME_RESOLUTION_YEAR:
                strftime(timestring, LEN_TIMESTRING, "%Y-XX-XXTXX:XX:XX%z", &t2);
                break;
        case FILENAME_TIME_RESOLUTION_MONTH:
                strftime(timestring, LEN_TIMESTRING, "%Y-%m-XXTXX:XX:XX%z", &t2);
                break;
        case FILENAME_TIME_RESOLUTION_DAY:
                strftime(timestring, LEN_TIMESTRING, "%Y-%m-%dTXX:XX:XX%z", &t2);
                break;
        case FILENAME_TIME_RESOLUTION_HOUR:
                strftime(timestring, LEN_TIMESTRING, "%Y-%m-%dT%H:XX:XX%z", &t2);
                break;
        case FILENAME_TIME_RESOLUTION_MINUTE:
                strftime(timestring, LEN_TIMESTRING, "%Y-%m-%dT%H:%M:XX%z", &t2);
                break;
        case FILENAME_TIME_RESOLUTION_SECOND:
        default:
                strftime(timestring, LEN_TIMESTRING, "%Y-%m-%dT%H:%M:%S%z", &t2);
        }
        sprintf(filename, "%s%s_ivcurve.csv", basepath, timestring);
}

#define WRITE_HEADINGS_AT_TOP_OF_FILE 1
#define NO_HEADINGS_AT_TOP_OF_FILE 0
static void write_data_to_file(char *filename, time_t reported_time, int writeheadings){
	int i, j;

        FILE *fp;

        struct stat st;
        int statresult;

        struct tm t2;
        char timestring[LEN_TIMESTRING];
	char headings[] = "unix_time,text_time,module_num,";
	

        localtime_r(&reported_time, &t2);


        statresult = stat(filename, &st);
        // statresult will be 0 if the file exists.
        // Could also check errno for more info.
        // if errno is ENOENT then the file does not exist,
        // if it is ENOTDIR then part of the path you provided is not a directory,
        // if it's EACCESS then you didn't have read permission on one of the directories in the path and so stat can't give you an answer

        if(statresult != 0){ // file doesn't exist, or another problem occurred
		if(errno == ENOENT){ // file does not exist
			// writes headings to data file
			fp = fopen(filename, "w");
			if(!fp){
				printf("failed to create output file.\n");
				return;
			}else{
				if(writeheadings == WRITE_HEADINGS_AT_TOP_OF_FILE){
					fprintf(fp, "%s", headings);
					for(i=0; i<NPOINTS; i++){
						fprintf(fp, ",V[%d]",i);
					}
					fprintf(fp, ",");
					for(i=0; i<NPOINTS; i++){
						fprintf(fp, ",I[%d]",i);
					}
					fprintf(fp, "\n");
				}
				fclose(fp);
			}
		}
	}
	// file should now exist, and will have header at the top, if required.

	fp = fopen(filename, "a");
	if(!fp){
		printf("failed to open output file for append.\n");
		return;
	}

        for (j = 0; j < NMODULES; j++) {
                if (mods[j].active){

			fprintf(fp, "%lld,", (long long int)reported_time);
			strftime(timestring, LEN_TIMESTRING, "%Y-%m-%dT%H:%M:%S%z", &t2);
			fprintf(fp, "%s,", timestring);
			fprintf(fp, "%d,", j);

			for(i=0; i<NPOINTS; i++){
				fprintf(fp, ",%f", (mods[j]).vdata[i]);
			}
			fprintf(fp, ",");
			for(i=0; i<NPOINTS; i++){
				fprintf(fp, ",%f", (mods[j]).idata[i]);
			}
			fprintf(fp, "\n");
		}
	}

	fclose(fp);
}


static void output_xml(){
	xmlDocPtr doc;
	xmlNodePtr ent;
	xmlNodePtr modent;
	char buf[200];

	doc = xmlNewDoc((xmlChar *) "1.0");
	doc->children = xmlNewDocNode(doc, NULL, (xmlChar *) "ivcurve", NULL);

	timestr(&thismeas, buf);
	ent = xmlNewChild(doc->children, NULL, (xmlChar *) "time", (xmlChar *) buf);

	int i;
	for (i = 0; i < NMODULES; i++) {
		sprintf(buf, "%d", i);
		modent = xmlNewChild(doc->children, NULL, (xmlChar *) "module", NULL);
		xmlSetProp(modent, (xmlChar *) "id", (xmlChar *) buf);

		sprintf(buf, "%d", mods[i].active);
		ent = xmlNewChild(modent, NULL,	(xmlChar *) "active", (xmlChar *) buf);

		ent = xmlNewChild(modent, NULL,	(xmlChar *) "name", (xmlChar *) mods[i].name);
		
		sprintf(buf, "%f", mods[i].voc);
		ent = xmlNewChild(modent, NULL,	(xmlChar *) "voc", (xmlChar *) buf);
		xmlSetProp(ent, (xmlChar *) "units", (xmlChar *) "V");

		sprintf(buf, "%f", mods[i].vmp);
		ent = xmlNewChild(modent, NULL,	(xmlChar *) "vmp", (xmlChar *) buf);
		xmlSetProp(ent, (xmlChar *) "units", (xmlChar *) "V");

		sprintf(buf, "%f", mods[i].isc);
		ent = xmlNewChild(modent, NULL,	(xmlChar *) "isc", (xmlChar *) buf);
		xmlSetProp(ent, (xmlChar *) "units", (xmlChar *) "A");

		sprintf(buf, "%f", mods[i].imp);
		ent = xmlNewChild(modent, NULL,	(xmlChar *) "imp", (xmlChar *) buf);
		xmlSetProp(ent, (xmlChar *) "units", (xmlChar *) "A");

		sprintf(buf, "%f", mods[i].mpp);
		ent = xmlNewChild(modent, NULL,	(xmlChar *) "mpp", (xmlChar *) buf);
		xmlSetProp(ent, (xmlChar *) "units", (xmlChar *) "W");
	}

	xmlSaveFile(XMLFN, doc);
	xmlFreeDoc(doc);
}

static int load_cfg(){
	FILE *f;
	int m;
	int active;
	char name[LEN_MOD_NAME+1];

	for(m=0; m<NMODULES; m++){
		mods[m].active = 0;
		strcpy(mods[m].name, "");
	}

	f = fopen(MODFN, "r");
	if (f == NULL) {
		logg(LOGFN, 1, "Opening config file failed.");
		return 1;
	}


	while (fscanf(f, "%d,%d,%" MSTR(LEN_MOD_NAME) "s\n", &m, &active, name) == 3) {
		if((m>=0)&&(m<=NMODULES)){
			mods[m].active = active;
			strcpy(mods[m].name, name);
		}else{
			// Bad module number, ignore that line of the config file.
			fprintf(stderr, "load_cfg: file %s contains a line with bad module number.\n", MODFN);
		}
	}

	fclose(f);
	return 0;
}

static int save_cfg(){
	int i;
	FILE *f;

	f = fopen(MODFN, "w");
	if (f == NULL) {
		logg(LOGFN, 1, "Opening config file failed.");
		return 1;
	}

	for (i = 0; i < NMODULES; i++) {
		fprintf(f, "%d,%d,%s\n", i, mods[i].active, mods[i].name);
	}

	fclose(f);
	return 0;
}


int main(int argc, char *argv[]){
	//char tbuf[21];
	//struct timespec slp_time;
	struct tm lt;
	int intmins;
	time_t wait_seconds;
	int err;

	logg(LOGFN, 0, "Starting up...");

	signal(SIGINT, sigint_handler);

	// Initialise mutex.
	pthread_mutex_init(&mtx, NULL);

	// Opening and starting socket communication.
	if (open_socket(SOCKET_PATH, msg_handler)) {
		pthread_mutex_destroy(&mtx);
		return 1;
	}

	if (start_socket()) {
		close_socket();
		pthread_mutex_destroy(&mtx);
		return 1;
	}

	if (load_cfg()) {
		logg(LOGFN, 1, "Failed to open config, using defaults.");
		if (save_cfg()) {
			logg(LOGFN, 1, "Failed to save config.");
		}
	}

	// open and reset PVPM.
	err = ksm_open(&ksm_portdata, ksm_devname, ksm_baudrate, ksm_timeout_10ths);
	if (err) {
		loggerr(LOGFN, "ksm_open error");
		close_socket();
		pthread_mutex_destroy(&mtx);
		return 1;
	}
	logg(LOGFN, 0, "debug : ksm is opened");


	err = mux_open(&mux_portdata, mux_devname, mux_baudrate, mux_timeout_10ths);
	if (err) {
		loggerr(LOGFN, "mux_open failed");
		close_socket();
		pthread_mutex_destroy(&mtx);
		return 1;
	}
	logg(LOGFN, 0, "debug : mux is opened");


	if (ksm_reset(&ksm_portdata)){
		logg(LOGFN, 1, "ksm_reset failed.");
		shutdown();
		return 1;
	}
	logg(LOGFN, 0, "debug : ksm is reset");

	if(ksm_beep_enable(&ksm_portdata, 0)){ // disable the beeping!
		logg(LOGFN, 1, "ksm_beep_enable(... , 0) failed.");
		shutdown();
		return 1;
	}
	logg(LOGFN, 0, "debug : ksm beeping disabled");

	if(ksm_remotesense_enable(&ksm_portdata, 1)){ // measure (and control) the voltage using the sense terminals
		logg(LOGFN, 1, "ksm_remotesense_enable(... , 1) failed.");
		shutdown();
		return 1;
	}
	logg(LOGFN, 0, "debug : ksm remotesensing enabled");

	// Don't start measuring until the minutes are an integer multiple of the measurement interval
	nextmeas = time(NULL);
	localtime_r(&nextmeas, &lt);
	intmins = (int) lt.tm_min/MEAS_INT + 1;
	intmins = intmins*MEAS_INT;
	wait_seconds = (intmins - lt.tm_min)*60 - lt.tm_sec;
	nextmeas = nextmeas + wait_seconds;

	while (1){
	  main_loop();
	}

	shutdown();
	return 0;
}

static void main_loop(){
	struct timespec slp_time;
	time_t currtime;
	time_t wait_seconds;

	currtime = time(NULL);
	wait_seconds = nextmeas - currtime;
	printf("Next measure time %s", ctime(&nextmeas));
	printf("Seconds to wait %d\n", (int) wait_seconds);

	if (wait_seconds < 0) {
		logg(LOGFN, 1, "Waiting time gone negative: %d\n", (int) wait_seconds);
		// Continue without sleeping.
		//  but skip the measurement that would be at the wrong time
	} else {
		slp_time.tv_sec = wait_seconds;
		slp_time.tv_nsec = 0;
		nanosleep(&slp_time, NULL);
		measure_channels();
	}

	// Next time to start measuring.
	nextmeas = nextmeas + MEAS_INT*60;
}

static void measure_channels(void){
	int j;
	int err;
	int mppindex;
	int k;
        char data_filename[LEN_FILENAME];

	thismeas = nextmeas;

	// Lock off global variables so socket cannot interfere.
	pthread_mutex_lock(&mtx);

	for (j = 0; j < NMODULES; j++) {
		mods[j].voc = 0.0;
		mods[j].isc = 0.0;
		mods[j].vmp = 0.0;
		mods[j].imp = 0.0;
		mods[j].mpp = 0.0;

		if (mods[j].active) {
			printf("Measuring module %d\n", j);
			logg(LOGFN, 0, "Measuring a module...");

			mux_set(&mux_portdata, j); // Select module j for testing

			// Zero the variables so that if they fail to be set the readings shouldn't be too misleading
			mods[j].vmp = mods[j].imp = mods[j].voc = mods[j].isc = mods[j].mpp = 0.0;
			for(k=0; k<NPOINTS; k++){
			  mods[j].vdata[k] = mods[j].idata[k] = 0.0;
			}

			// Start curve trace...
			err = ksm_measure_autorange_iv(&ksm_portdata, NPOINTS, &(mods[j].voc), &(mods[j].isc), mods[j].vdata, mods[j].idata, &(mods[j].meastime));
			//pvpm_measure(pvpm, mods[j].ivdata)
			if (err){
				logg(LOGFN, 1, "ksm_measure_auorange_iv failed.");
				break;
			}else{
				mppindex = calc_mpp_index(NPOINTS, mods[j].isc*mods[j].voc, mods[j].vdata, mods[j].idata);

				mods[j].vmp = mods[j].vdata[mppindex];
				mods[j].imp = mods[j].idata[mppindex];
				// assumed isc*voc has the right sign for power being generated
				mods[j].mpp = mods[j].vdata[mppindex]*mods[j].idata[mppindex];
			}
		}
	}

	mux_set(&mux_portdata, MUX_ALL_OFF);

	logg(LOGFN, 0, "Total measurement time: %d (sec)", (int) (time(NULL) - thismeas));

#ifdef ADD_RESULTS_TO_DATABASE
	// Insert data into database.
	for (j = 0; j < NMODULES; j++) {
		if (mods[j].active){
			err = database_module(j, thismeas, mods[j]);
		}
	}
#endif

	create_filename(data_filename, DATAFN_ARCHIVEPATH, thismeas, FILENAME_TIME_RESOLUTION_DAY);
	write_data_to_file(data_filename, thismeas, WRITE_HEADINGS_AT_TOP_OF_FILE);

	create_filename(data_filename, DATAFN_PROCESSPATH, thismeas, FILENAME_TIME_RESOLUTION_SECOND);
	write_data_to_file(data_filename, thismeas, WRITE_HEADINGS_AT_TOP_OF_FILE);

	// Ouput data to XML file.
	output_xml();
	pthread_mutex_unlock(&mtx); // Socket communication can now take place
}

static void msg_handler(const char *msg, char *rply){
	// This function is called by thread so need to make sure
	// any data it accesses is done in thread safe manner.

	char cmd[100];
	int nconv;
	int mod;
	int act;
	char name[LEN_MOD_NAME+1];

	printf("Entering msg_handler\n");
	nconv = sscanf(msg, "iv_cmd %s", cmd);
	if (nconv == 0) {
		strcpy(rply, ERR_CMD);
		return;
	}
	printf("Command: %s\n", cmd);

	// Locking global variables.
	pthread_mutex_lock(&mtx);
	if (strcmp(cmd, "set") == 0) {
		nconv = sscanf(msg, "iv_cmd set %d %d", &mod, &act);
		if (nconv == 2) {
			printf("Set %d to %d\n", mod, act);
			if (mod >= 0 && mod < NMODULES) {
				mods[mod].active = act;
				strcpy(rply, ACK);
			} else {
				strcpy(rply, ERR_SET);
			}
		} else {
			strcpy(rply, ERR_SET);
		}
	} else if (strcmp(cmd, "name") == 0) {
		nconv = sscanf(msg, "iv_cmd name %d %" MSTR(LEN_MOD_NAME) "s", &mod, name);
		if (nconv == 2) {
			printf("Set name of %d to %s\n", mod, name);
			if (mod >= 0 && mod < NMODULES) {
				strcpy(mods[mod].name, name);
				strcpy(rply, ACK);
			} else {
				strcpy(rply, ERR_SET);
			}
		} else {
			strcpy(rply, ERR_SET);
		}
	} else {
		strcpy(rply, ERR_CMD);
	}

	if (save_cfg()) {
		logg(LOGFN, 1, "Failed to save config.");
	}
	output_xml();
	pthread_mutex_unlock(&mtx);
}

static void shutdown(){
	logg(LOGFN, 0, "Shutting down.");

	ksm_close(&ksm_portdata);
	mux_close(&mux_portdata);

	close_socket();

	pthread_mutex_destroy(&mtx);
}

static void sigint_handler(){
	shutdown();

	signal(SIGINT, SIG_DFL); // Reattach quiting handler for SIGINT
	kill(getpid(), SIGINT);
}


#ifdef ADD_RESULTS_TO_DATABASE
// Return 0 for success, nonzero for failure
static int database_module(const int j, const time_t time, const struct module mod){
	PGconn *con;
	char buf[200];
	int timeid;
	int nameid;
	int valueid;

	int result;
	int count = 0;

	double ivdata_tmp[NPOINTS*2]; // match old format with 101 voltage readings then 101 current readings
	int k;

	con = db_open();
	if (con == NULL) {
		logg(LOGFN, 1, "Connection to database failed.");
		return 1;
	}

	if (db_gen_timeid(con, &timeid, time)) {
		logg(LOGFN, 1, "db_gen_timeid failed.");
		return 2;
	}

	// Insert voc.
	sprintf(buf, "Mod%d_Voc", j);
	if (db_gen_nameid(con, &nameid, buf, "V")) {
		logg(LOGFN, 1, "db_gen_nameid failed.");
		return 3;
	}
	if (db_gen_valueid(con, &valueid, &mod.voc, 1)) {
		logg(LOGFN, 1, "db_gen_valueid failed.");
		return 4;
	}
	result = db_insert_record(con, timeid, nameid, valueid);
	if (result == 1) {
		logg(LOGFN, 1, "db_insert_record failed.");
		return 5;
	} else if (result == 0) {
		count++;
	}

	// Insert isc.

	sprintf(buf, "Mod%d_Isc", j);
	if (db_gen_nameid(con, &nameid, buf, "A")) {
		logg(LOGFN, 1, "db_gen_nameid failed.");
		return 6;
	}
	if (db_gen_valueid(con, &valueid, &mod.isc, 1)) {
		logg(LOGFN, 1, "db_gen_valueid failed.");
		return 7;
	}
	result = db_insert_record(con, timeid, nameid, valueid);
	if (result == 1) {
		logg(LOGFN, 1, "db_insert_record failed.");
		return 8;
	} else if (result == 0) {
		count++;
	}

	// Insert mpp.
	sprintf(buf, "Mod%d_Pmpp", j);
	if (db_gen_nameid(con, &nameid, buf, "W")) {
		logg(LOGFN, 1, "db_gen_nameid failed.");
		return 9;
	}
	if (db_gen_valueid(con, &valueid, &mod.mpp, 1)) {
		logg(LOGFN, 1, "db_gen_valueid failed.");
		return 10;
	}
	result = db_insert_record(con, timeid, nameid, valueid);
	if (result == 1) {
		logg(LOGFN, 1, "db_insert_record failed.");
		return 11;
	} else if (result == 0) {
		count++;
	}

	if(fabs(mod.isc) >= MIN_ISC_FOR_IVCURVE){
		for(k=0; k<NPOINTS; k++){
			ivdata_tmp[k]         = mod.vdata[k];
			ivdata_tmp[k+NPOINTS] = mod.idata[k];
		}

		// Insert curve.
		sprintf(buf, "Mod%d_Curve", j);
		if (db_gen_nameid(con, &nameid, buf, "V:A")) {
			logg(LOGFN, 1, "db_gen_nameid failed.");
			return 12;
		}
		if (db_gen_valueid(con, &valueid, ivdata_tmp, 2*NPOINTS)) {
			logg(LOGFN, 1, "db_gen_valueid failed.");
			return 13;
		}
		result = db_insert_record(con, timeid, nameid, valueid);
		if (result == 1) {
			logg(LOGFN, 1, "db_insert_record failed.");
			return 14;
		} else if (result == 0) {
			count++;
		}
	}

	db_close(con);
	logg(LOGFN, 0, "%d entries added to database.\n", count);

	return 0;
}
#endif
