#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <math.h>
#include <stdlib.h>

#include "cj_serial.h"
#include "ksm_control.h"


#define NPOINTS 101
int main(void){
  const char devname[] = "/dev/ttyUSB-sourcemeter";
  const long int baudrate = 57600;
  //const long int baudrate = 38400;
  //const long int baudrate = 9600;

  const int timeout_10ths = 10;
  cj_serial_t portmem;
  cj_serial_t *portdata;
  int err;
  //  unsigned char inpbuf[KSM_INPBUFLEN];
  //  ssize_t nread;
  //  int i;
  double voc;
  double isc;
  double varray[NPOINTS];
  double iarray[NPOINTS];
  struct timeval meastime;

  portdata = &portmem;

  if((err = ksm_open(portdata, devname, baudrate, timeout_10ths))) return 1;

  err = ksm_reset(portdata);
  printf("ksm_reset() returned %d\n", err);

  err = ksm_check_connection(portdata);
  printf("ksm_check_connection() returned %d\n", err);

  //  err = ksm_beep_enable(portdata, 0);
  //  printf("ksm_beep_enable() returned %d\n", err);


  //  err = ksm_display_enable(portdata, 0);
  //  printf("ksm_display_enable() returned %d\n", err);

  //  err = ksm_measure_fixedrange_voc(portdata, 100.0, &voc);
  //  printf("ksm_measure_fixedrange_voc() returned %d\n", err);
  //  if(!err) printf("VOC = %lf\n", voc);

    err = ksm_measure_autorange_voc(portdata, &voc);
    printf("ksm_measure_autorange_voc() returned %d\n", err);
    if(!err) printf("VOC = %lf\n", voc);


    //  err = ksm_measure_fixedrange_isc(portdata, 1.0, &isc);
    //  printf("ksm_measure_fixedrange_isc() returned %d\n", err);
    //  if(!err) printf("ISC = %lf\n", isc);

  err = ksm_measure_autorange_isc(portdata, &isc);
  printf("ksm_measure_autorange_isc() returned %d\n", err);
  if(!err) printf("ISC = %lg\n", isc);

  err = ksm_measure_autorange_iv(portdata, NPOINTS, &voc, &isc, varray, iarray, &meastime);
  printf("ksm_measure_autorange_iv() returned %d\n", err);
  if(!err) printf("VOC = %lf, \tISC = %lg\n", voc, isc);

  err = ksm_measure_autorange_iv(portdata, NPOINTS, &voc, &isc, varray, iarray, &meastime);
  printf("ksm_measure_autorange_iv() returned %d\n", err);
  if(!err) printf("VOC = %lf, \tISC = %lg\n", voc, isc);
  ksm_close(portdata);

  int mppi;

  mppi =  calc_mpp_index(NPOINTS, isc, varray, iarray);
  printf("MPPT point %d, V=%lf, I=%lf, P=%lf, FF=%lf\n", mppi, varray[mppi], iarray[mppi], varray[mppi]*iarray[mppi],  varray[mppi]*iarray[mppi]/(voc*isc));


  printf("meastime %ld . %ld \n", (long int)meastime.tv_sec, (long int)meastime.tv_usec);

  return 0;
}



// Bugs:
// Sourcemeter sometimes returns error code:<-113,"Undefined header">
// If the current increases suddenly during autorange measurement, it will fail
// If the voltage increases suddenly during autorange measurement, it will fail

// Todo:
// Record time at start and end of measurement
// ?Perhaps re-measure VOC after sweep to make it match the curve better
// ?Speed optimisation?
// ?Reduce error handling code?
// Build functions into main program that stores results in database
// Make it resilient to error codes from sourcemeter (auto restart in case of error?)
// Make it resilient to temporary / intermittent failure of USB connection to USB-Serial converter
// ? make it put higher density of points around interesting region of IV curve?



// DONE * Make sure number of points is always as requested
