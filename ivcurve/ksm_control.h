#ifndef INC_KSM_CONTROL_H
#define INC_KSM_CONTROL_H

// Open a serial port for the sourcemeter
// timeout is the time in 1/10 s to wait before returning if less data arrives than was supposed to.
// the timeout is applied between characters, effectively. There may be multiple gaps each less than
// timeout and it won't return even if the sum of the gaps exceeds timeout.
// returns -1 on error, or 0 on success
int ksm_open(cj_serial_t *portdata, const char *device_name, long int baudrate, int timeout_10ths);

// shutdown the Keithley Sourcemeter and close the serial port
void ksm_close(cj_serial_t *portdata);

// Send an identification command to check whether the Keithley Sourcemeter is present, connected and switched on
// Returns 0 if it is OK, returns 1 for no response
int ksm_check_connection(cj_serial_t *portdata);

// enable or disable beeping
int ksm_beep_enable(cj_serial_t *portdata, int enable);

int ksm_remotesense_enable(cj_serial_t *portdata, int enable);

// Waits for the timeout to expire with no new output from sourcemeter, then
// resets the Sourcemeter
int ksm_reset(cj_serial_t *portdata);

int ksm_measure_autorange_voc(cj_serial_t *portdata, double *voc);

int ksm_measure_autorange_isc(cj_serial_t *portdata, double *isc);

// Measures the open circuit voltage then measures the short-circuit current, then measures the IV curve.
// It records the time just before the IV curve in *meastime, unless meastime is a null pointer
int ksm_measure_autorange_iv(cj_serial_t *portdata, int npoints, double *voc, double *isc, double *v, double *i, struct timeval *meastime);

// From an IV curve, finds the maximum power point and returns the index of that point.
// sign determines the direction of the power flow that is the direction we are looking for.
// If pwrsign = 1, then the point found will have the most positive v * i.
// If the i values are negative, then pwrsign = -1 will probably be wanted
int calc_mpp_index(int npoints, double pwrsign, double *v, double *i);

#endif /* INC_KSM_CONTROL_H */

