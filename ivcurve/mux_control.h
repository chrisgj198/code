#ifndef INC_MUX_CONTROL_H
#define INC_MUX_CONTROL_H


// To control relay mux

// This channel number turns off every relay
#define MUX_ALL_OFF 9

// Channels are 0 ... 7
#define MUX_NCHANNELS 8

// returns 0 on success, non-zero on failure
int mux_set(cj_serial_t *portdata, unsigned int channel);

// returns 0 on success, non-zero on failure
int mux_open(cj_serial_t *portdata, const char *device_name, long int baudrate, int timeout_10ths);

void mux_close(cj_serial_t *portdata);

#endif
