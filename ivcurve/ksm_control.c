#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <math.h>
#include <stdlib.h>

#include "cj_serial.h"
#include "ksm_control.h"

//#define KSM_CHECK_ALL_COMMANDS

#define KSM_INPBUFLEN 256
#define KSM_COMMANDLEN 256

int ksm_open(cj_serial_t *portdata, const char *device_name, long int baudrate, int timeout_10ths){
  int err;

  // Open a serial port
  // timeout is the time in 1/10 s to wait before returning if less data arrives than was supposed to.
  // the timeout is applied between characters, effectively. There may be multiple gaps each less than
  // timeout and it won't return even if the sum of the gaps exceeds timeout.
  // returns -1 on error, or 0 on success
  err = cj_serial_open(portdata, device_name, baudrate, 0, 1, timeout_10ths);
  // 0 is no HW flow control,
  // 1 is enable xon/xoff flow control

  return err;
}


// Take a command, add any end of line characters and send it
int ksm_send_text(cj_serial_t *portdata, char *string){
  char txt[KSM_COMMANDLEN];
  const char suffix[] = "\r";
  //const char suffix[] = "\n\r";
  int retval;

  if((strlen(string) + strlen(suffix) + 1) > KSM_COMMANDLEN){
    fprintf(stderr, "KSM_COMMANDLEN insuffucient\n");
    return 1;
  }

  sprintf(txt, "%s%s", string, suffix);
  retval = cj_serial_write(portdata, txt);
  if(retval) return retval;

  //int d;
  //scanf("%d", &d);
  //  usleep(1000000L);
  cj_serial_wait_transmit_done(portdata); // wait for request to be sent
  return retval;
}

// Don't query status after sending text, because there is a measurement reply coming instead.
int ksm_send_query(cj_serial_t *portdata, char *string){
  int err;

  if((err = ksm_send_text(portdata, string))) return err;
  return 0;
}


#define KSM_STATUS_OSB 0x80
#define KSM_STATUS_MSS 0x40
#define KSM_STATUS_ESB 0x20
#define KSM_STATUS_MAV 0x10
#define KSM_STATUS_QSB 0x08
#define KSM_STATUS_EAV 0x04
#define KSM_STATUS_MSB 0x01
// Request the status of the Sourcemeter
int ksm_get_status(cj_serial_t *portdata, int *ksm_status){
  char inpbuf[KSM_INPBUFLEN];
  ssize_t nread, index;
  int nconv;
  int err;

  cj_serial_flush_input(portdata);

  err = ksm_send_query(portdata, "*STB?");
  if(err){
    fprintf(stderr, "ksm_get_status(): ksm_send_command() failed\n");
    return 1;
  }

  // Receive the status message
  nread = cj_serial_read_to_delimeter(portdata, inpbuf, KSM_INPBUFLEN-1, 2, "\r\n");
  if(nread < 0){
    fprintf(stderr, "ksm_get_status(): cj_serial_read_to_delimeter() failed.\n");
    return 1;
  }
  if(nread>0)nread--; // Remove the last character which is hopefully a delimeter
  inpbuf[nread] = 0; // Null terminate reply string
  //  printf("nread = %d\n", (int)nread);

  if(nread > 3) return 1; // If more than a 3 digit number, this isn't a status byte in decimal.

  for(index=0; index<nread; index++){
    if(inpbuf[index] < '0') return 1; // return an error if the status byte contains non-numerical values
    if(inpbuf[index] > '9') return 1;
  }

  nconv = sscanf(inpbuf, "%d", ksm_status);
  if(nconv != 1) return 1; // if we didn't convert one value, there is an error

  return 0;
}

// Request & print an error from the error queue of the Sourcemeter
int ksm_print_error(cj_serial_t *portdata){
  char inpbuf[KSM_INPBUFLEN];
  ssize_t nread;
  int err;

  cj_serial_flush_input(portdata);

  err = ksm_send_query(portdata, "STAT:QUE?");
  if(err){
    fprintf(stderr, "ksm_print_error(): ksm_send_query() failed\n");
    return 1;
  }

  // Receive the status message
  nread = cj_serial_read_to_delimeter(portdata, inpbuf, KSM_INPBUFLEN-1, 2, "\r\n");
  if(nread < 0){
    fprintf(stderr, "ksm_print_error(): cj_serial_read_to_delimeter() failed.\n");
    return 1;
  }
  if(nread>0)nread--; // Remove the last character which is hopefully a delimeter
  inpbuf[nread] = 0; // Null terminate reply string

  fprintf(stderr, "Sourcemeter returned error code:<%s>\n", inpbuf);

  //nconv = sscanf(inpbuf, "%d", ksm_status);
  //if(nconv != 1) return 1; // if we didn't convert one value, there is an error

  return 0;
}


int ksm_check_status(cj_serial_t *portdata){
  int ksm_status;

  if(ksm_get_status(portdata, &ksm_status)) return 1;
  if(ksm_status){
    // printf("KSM status  %x\n", ksm_status);
    if(ksm_status & KSM_STATUS_EAV){ // error available
      ksm_print_error(portdata);
      ksm_send_text(portdata, ":OUTP OFF");

      return 1;
    }
  }
  return 0;
}


// Send a command (no reply expected)
// optionally checking the status after the command has been send
int ksm_send_command(cj_serial_t *portdata, char *string){
  int err;

  if((err = ksm_send_text(portdata, string))) return err;

  #ifdef KSM_CHECK_ALL_COMMANDS
  if((err = ksm_check_status(portdata))) return err;
  #endif

  return 0;
}



// shutdown the Keithley Sourcemeter and close the serial port
void ksm_close(cj_serial_t *portdata){
  int err;
  err = ksm_send_command(portdata, ":OUTP OFF");
  if(err) fprintf(stderr, "ksm_close(): ksm_send_command() failed\n");
  cj_serial_wait_transmit_done(portdata); // This doesn't seem to wait long enough
  cj_serial_close(portdata);
}

// Send an identification command to check whether the Keithley Sourcemeter is present, connected and switched on
// Returns 0 if it is OK, returns 1 for no response
int ksm_check_connection(cj_serial_t *portdata){
  // The sourcemeter should reply with the following, terminated by a <CR>
  //  const char expectedreply[] = "KEITHLEY INSTRUMENTS INC.,MODEL 2430,1272129,C30   Mar 17 2006 09:29:29/A02  /E/L\r";
  const char expectedreply[] = "KEITHLEY INSTRUMENTS INC.,MODEL 2430,1272129,C30   Mar 17 2006 09:29:29/A02  /E/L";
  char inpbuf[KSM_INPBUFLEN];
  ssize_t nread;
  int err;

  cj_serial_flush_input(portdata);

  // Request instrument ID
  err = ksm_send_query(portdata, "*IDN?");
  if(err){
    fprintf(stderr, "ksm_check_connection(): ksm_send_query() failed\n");
    return 1;
  }
  cj_serial_wait_transmit_done(portdata); // wait for request to be sent

  if(strlen(expectedreply)>KSM_INPBUFLEN-1){
    fprintf(stderr, "ksm_check_connection(): KSM_INPBUFLEN too small\n");
    return 1;
  }

  nread = cj_serial_read_to_delimeter(portdata, inpbuf, KSM_INPBUFLEN-1, 2, "\r\n");
  if(nread < 0){
    fprintf(stderr, "ksm_check_connection(): cj_serial_read_to_delimeter() failed.\n");
    return 1;
  }
  if(nread>0)nread--; // Remove the last character which is hopefully a delimeter
  inpbuf[nread] = 0; // Null terminate reply string
  if(strcmp(expectedreply, inpbuf)){ // if reply differs from expected reply
    printf("%s\n", inpbuf);
    return 1; // error
  }
  return 0; // ok, reply matched expected reply
}

int ksm_reset(cj_serial_t *portdata){
  int err;
  ssize_t nread;
  char inpbuf[KSM_INPBUFLEN];

  cj_serial_flush_output(portdata);
  cj_serial_flush_input(portdata); // Flush data buffered in the PC

  do{
    nread = cj_serial_read(portdata, inpbuf, KSM_INPBUFLEN-1);
    // read until either timeout expires, or characters are received

  }while(nread > 0); // Only stop looping when there are no characters from the Sourcemeter
  // By now, the buffer in the sourcemeter should have emptied

  // Send just a newline, to end any invalid line in progress
  err = ksm_send_text(portdata, "");
  if(err){
    fprintf(stderr, "ksm_reset(): ksm_send_text() failed\n");
    return 1;
  }

  // Reset sourcemeter
  err = ksm_send_text(portdata, "*RST"); // Don't check status, so using ksm_send_text()
  if(err){
    fprintf(stderr, "ksm_reset(): ksm_send_text() failed\n");
    return 1;
  }

  usleep(300000L); // Give the Sourcemeter time to reset itself

  // Clear error queue
  err = ksm_send_text(portdata, ":STAT:QUE:CLE");
  if(err){
    fprintf(stderr, "ksm_reset(): ksm_send_text() failed\n");
    return 1;
  }

  if((err = ksm_check_status(portdata))) return err;

  return 0;
}


int ksm_display_enable(cj_serial_t *portdata, int enable){
  int err;

  cj_serial_flush_input(portdata);
  if(enable){
    err = ksm_send_command(portdata, ":DISP:ENAB 1");
  }else{
    err = ksm_send_command(portdata, ":DISP:ENAB 0");
  }
  if(err){
    fprintf(stderr, "ksm_display_enable(): ksm_send_command() failed\n");
    return 1;
  }
  cj_serial_wait_transmit_done(portdata); // wait for request to be sent

  return 0;
}


int ksm_beep_enable(cj_serial_t *portdata, int enable){
  int err;

  cj_serial_flush_input(portdata);
  if(enable){
    err = ksm_send_command(portdata, ":SYSTEM:BEEP:STATE ON");
  }else{
    err = ksm_send_command(portdata, ":SYSTEM:BEEP:STATE OFF");
  }
  if(err){
    fprintf(stderr, "ksm_beep_enable(): ksm_send_command() failed\n");
    return 1;
  }
  cj_serial_wait_transmit_done(portdata); // wait for request to be sent

  return 0;
}

int ksm_remotesense_enable(cj_serial_t *portdata, int enable){
  int err;

  cj_serial_flush_input(portdata);
  if(enable){
    err = ksm_send_command(portdata, ":SYSTEM:RSEN ON");
  }else{
    err = ksm_send_command(portdata, ":SYSTEM:RSEN OFF");
  }
  if(err){
    fprintf(stderr, "ksm_remotesense_enable(): ksm_send_command() failed\n");
    return 1;
  }
  cj_serial_wait_transmit_done(portdata); // wait for request to be sent

  return 0;
}


double get_eqhigher_irange(double ival){
  ival = fabs(ival);
  if(ival > 3.0){
    fprintf(stderr, "Danger: trying to measure currents outside the capability of the Keithley 2430 sourcemeter.\n");
    return 3.0;
  }else{
    if(ival > 1.0){
      // 1A to 3A
      return 3.0;
    }else{
      if(ival > 100e-3){
	// 100mA to 1A
	return 1.0;
      }else{
	if(ival > 10e-3){
	  // 10mA to 100mA
	  return 100e-3;
	}else{
	  if(ival > 1e-3){
	    // 1mA to 10mA
	    return 10e-3;
	  }else{
	    if(ival > 100e-6){
	      // 100uA to 1mA
	      return 1e-3;
	    }else{
	      if(ival > 10e-6){
		// 10uA to 100uA
		return 100e-6;
	      }else{
		// up to 10uA
		return 10e-6;
	      }
	    }
	  }
	}
      }
    }
  }
}

double get_next_lower_irange(double ival){
  ival = fabs(ival);
  if(ival > 3.0){
    fprintf(stderr, "Danger: trying to measure currents outside the capability of the Keithley 2430 sourcemeter.\n");
    return 3.0;
  }else{
    if(ival > 1.0){
      // >1A, <=3A
      return 1.0;
    }else{
      if(ival > 100e-3){
	// >100mA, <=1A
	return 100e-3;
      }else{
	if(ival > 10e-3){
	  // >10mA, <=100mA
	  return 10e-3;
	}else{
	  if(ival > 1e-3){
	    // >1mA, <=10mA
	    return 1e-3;
	  }else{
	    if(ival > 100e-6){
	      // >100uA, <=1mA
	      return 100e-6;
	    }else{
	      // <100uA
	      return 10e-6;
	    }
	  }
	}
      }
    }
  }
}

double get_eqhigher_vrange(double vval){
  vval = fabs(vval);
  if(vval > 100){
    fprintf(stderr, "Danger: trying to measure voltages outside the capability of the Keithley 2430 sourcemeter.\n");
    return 100;
  }else{
    if(vval > 20.0){
      return 100.0;
    }else{
      if(vval > 2.0){
	return 20.0;
      }else{
	if(vval > 200e-3){
	  return 2.0;
	}else{
	  return 200e-3;
	}
      }
    }
  }
}

double get_next_lower_vrange(double vval){
  vval = fabs(vval);
  if(vval > 100){
    fprintf(stderr, "Danger: trying to measure voltages outside the capability of the Keithley 2430 sourcemeter.\n");
    return 100;
  }else{
    if(vval > 20.0){
      // vval <= 100V and vval >20V
      return 20.0;
    }else{
      // vval <= 20V
      if(vval > 2.0){
	// vval <= 20V and vval > 2.0
	return 2.0;
      }else{
	// vval <= 2V
	return 200e-3;
      }
    }
  }
}

int ksm_measure_fixedrange_voc(cj_serial_t *portdata, double range, double *voc){
  char txt[KSM_COMMANDLEN];
  char inpbuf[KSM_INPBUFLEN];
  ssize_t nread;
  int nconv;
  int err;

  if(fabs(range) > 100){
    fprintf(stderr, "Error: attempting to measure voltage range above capabilities of Keithley 2430.\n");
    return 1;
  }

  if(ksm_send_command(portdata, ":OUTP OFF")) return 1;
  if(ksm_send_command(portdata, ":SOUR:FUNC CURR")) return 1;
  if(ksm_send_command(portdata, ":SOUR:CURR:MODE FIXED")) return 1;
  if(ksm_send_command(portdata, ":SENS:FUNC \"VOLT\"")) return 1;
  if(ksm_send_command(portdata, ":SOUR:CURR:RANG MIN")) return 1;
  if(ksm_send_command(portdata, ":SOUR:CURR:LEV 0")) return 1;

  sprintf(txt, ":SENS:VOLT:PROT %lg", fabs(range));
  if(ksm_send_command(portdata, txt)) return 1;

  sprintf(txt, ":SENS:VOLT:RANG %lg", fabs(range));
  if(ksm_send_command(portdata, txt)) return 1;

  if(ksm_send_command(portdata, ":FORM:ELEM VOLT")) return 1;

  // Check for errors before turning on the output
  if((err = ksm_check_status(portdata))) return err;

  if(ksm_send_command(portdata, ":OUTP ON")) return 1;
  cj_serial_flush_input(portdata);
  if(ksm_send_query(portdata, ":READ?")) return 1;
  cj_serial_wait_transmit_done(portdata); // wait for request to be sent
  nread = cj_serial_read_to_delimeter(portdata, inpbuf, KSM_INPBUFLEN-1, 2, "\r\n");
  if(ksm_send_command(portdata, ":OUTP OFF")) return 1;

  if(nread < 0) return 1;

  if(nread>0)nread--; // Remove the last character which is hopefully a delimeter
  inpbuf[nread] = 0; // Null terminate reply string

  nconv = sscanf(inpbuf, "%lf", voc);
  if(nconv != 1) return 1; // if we didn't convert one value, there is an error

  return 0;
}

int ksm_measure_autorange_voc(cj_serial_t *portdata, double *voc){
  double voctmp;
  double range, newrange, tmprange;

  *voc = 1e6; // return a high value if we fail to measure it

  newrange = 100;

  do{
    range = newrange;
    if(ksm_measure_fixedrange_voc(portdata, range, &voctmp)) return 1;

    if(voctmp < range){
      *voc = voctmp;
    }else{
      return 1;
    }

    // should move down one range at a time without skipping ranges, to avoid mistakes
    // e.g. if we do a measurement on the 3A range and get a reading of 99uA,
    // this alone would not be sufficiently accurate information to choose
    // the 100uA range for the final measurement.

    newrange = get_eqhigher_vrange(voctmp * 1.05); // range we probably need
    tmprange = get_next_lower_vrange(range); // one range down from last measuremet
    if(tmprange > newrange) newrange = tmprange; // If we were going to skip ranges, don't
    
  }while(newrange < range);  
  return 0;
}

int ksm_measure_fixedrange_isc(cj_serial_t *portdata, double range, double *isc){
  char txt[KSM_COMMANDLEN];
  char inpbuf[KSM_INPBUFLEN];
  ssize_t nread;
  int nconv;
  double uprange;
  int err;

  if(fabs(range) > 3.0){
    fprintf(stderr, "Error: attempting to measure current range above capabilities of Keithley 2430.\n");
    return 1;
  }

  uprange = get_eqhigher_irange(range);

  if(ksm_send_command(portdata, ":OUTP OFF")) return 1;
  if(ksm_send_command(portdata, ":SOUR:FUNC VOLT")) return 1;
  if(ksm_send_command(portdata, ":SOUR:VOLT:MODE FIXED")) return 1;
  if(ksm_send_command(portdata, ":SOUR:VOLT:RANG 0.2")) return 1;
  if(ksm_send_command(portdata, ":SOUR:VOLT:LEV 0")) return 1;

  if(ksm_send_command(portdata, ":SENS:FUNC \"CURR\"")) return 1;

  sprintf(txt, ":SENS:CURR:PROT %lg", uprange);
  if(ksm_send_command(portdata, txt)) return 1;

  sprintf(txt, ":SENS:CURR:RANG %lg", uprange);
  if(ksm_send_command(portdata, txt)) return 1;

  if(ksm_send_command(portdata, ":FORM:ELEM CURR")) return 1;

  // Check for errors before turning on the output
  if((err = ksm_check_status(portdata))) return err;

  if(ksm_send_command(portdata, ":OUTP ON")) return 1;
  cj_serial_flush_input(portdata);
  if(ksm_send_query(portdata, ":READ?")) return 1;
  cj_serial_wait_transmit_done(portdata); // wait for request to be sent
  nread = cj_serial_read_to_delimeter(portdata, inpbuf, KSM_INPBUFLEN-1, 2, "\r\n");
  if(ksm_send_command(portdata, ":OUTP OFF")) return 1;
  if(nread < 0) return 1;

  if(nread>0) nread--; // Remove the last character which is hopefully a delimeter
  inpbuf[nread] = 0; // Null terminate reply string

  nconv = sscanf(inpbuf, "%lf", isc);
  if(nconv != 1) return 1; // if we didn't convert one value, there is an error

  return 0;
}


int ksm_measure_autorange_isc(cj_serial_t *portdata, double *isc){
  double isctmp;
  double range, newrange, tmprange;

  *isc = 1e6; // return a high value if we fail to measure it

  newrange = 3.0;

  do{
    range = newrange;
    if(ksm_measure_fixedrange_isc(portdata, range, &isctmp)) return 1;

    if(isctmp < range){
      *isc = isctmp;
    }else{
      return 1;
    }

    //FIXME : cam return 1 (fail) if current increases between choosing range and measurement.

    newrange = get_eqhigher_irange(isctmp * 1.05); // range we probably need
    tmprange = get_next_lower_irange(range); // one range down from last measuremet
    if(tmprange > newrange) newrange = tmprange; // If we were going to skip ranges, don't

  }while(newrange < range);

  return 0;
}

#define KSM_PRE_POINTS 2
#define KSM_POST_POINTS 3
#define KSM_EXTRA_POINTS 1
// Extra point is added because sourcemeter sometimes computes one less point due to
//  rounding errors converting from number of points to start, stop and step voltages and back.
//  If there are too many points, the last points will get discarded.

// Measures the open circuit voltage then measures the short-circuit current, then measures the IV curve.
// It records the time just before the IV curve in *meastime, unless meastime is a null pointer
int ksm_measure_autorange_iv(cj_serial_t *portdata, int npoints, double *voc, double *isc, double *v, double *i, struct timeval *meastime){
  double vstart, vstop, vstep;
  //double vrange;
  double imax;
  double val;
  int index, param;
  int nread, nconv;
  int ksm_npoints;
  char txt[KSM_COMMANDLEN];
  char inpbuf[KSM_INPBUFLEN];
  int speedup;
  int err;

  speedup = 0x01 | 0x02 | 0x04 | 0x08 | 0x10;
  // if bit 0x01 set, turn off display
  // if bit 0x02 set, turn off autorange
  // if bit 0x04 set, turn off autozero
  // if bit 0x08 set, set NPLC to 0.01
  // if bit 0x10 set, turn off averaging

  if(speedup & 0x01) if(ksm_display_enable(portdata, 0)) return 1;

  if(ksm_measure_autorange_voc(portdata, voc)) return 1;
  if(fabs(*voc) > 100){
    fprintf(stderr, "Error: attempting to measure voltage range above capabilities of Keithley 2430.\n");
    return 1;
  }

  if(ksm_measure_autorange_isc(portdata, isc)) return 1;
  if(fabs(*isc) > (((*voc) > 20.0)?1.0:3.0)){ // current limit is lower if the voltage can exceed 20V
    fprintf(stderr, "Error: attempting to measure current range above capabilities of Keithley 2430.\n");
    return 1;
  }

  if(ksm_send_command(portdata, ":OUTP OFF")) return 1;
  if(ksm_send_command(portdata, ":SOUR:FUNC VOLT")) return 1;
  if(ksm_send_command(portdata, ":SENS:FUNC \"VOLT\"")) return 1;
  if(ksm_send_command(portdata, ":SENS:FUNC \"CURR\"")) return 1;

  imax = (*isc) * 1.05;
  sprintf(txt, ":SENS:CURR:PROT %lg", get_eqhigher_irange(fabs(imax)));
  if(ksm_send_command(portdata, txt)) return 1;

  sprintf(txt, ":SENS:CURR:RANG %lg", get_eqhigher_irange(fabs(imax)));
  if(ksm_send_command(portdata, txt)) return 1;

  // Try to add PRE_POINTS past 0V and POST_POINTS past 0V
  vstep = (*voc) / ((npoints + KSM_EXTRA_POINTS) - KSM_PRE_POINTS - KSM_POST_POINTS - 1);
  vstart = -vstep * KSM_PRE_POINTS;
  vstop = (*voc) + vstep * KSM_POST_POINTS;

  sprintf(txt, ":SOUR:VOLT:START %lf", vstart);
  if(ksm_send_command(portdata, txt)) return 1;

  sprintf(txt, ":SOUR:VOLT:STOP %lf", vstop);
  if(ksm_send_command(portdata, txt)) return 1;

  sprintf(txt, ":SOUR:VOLT:STEP %lg", vstep);
  if(ksm_send_command(portdata, txt)) return 1;

  if(ksm_send_command(portdata, ":SOUR:VOLT:MODE SWE")) return 1;
  if(speedup & 0x02){
    if(ksm_send_command(portdata, ":SOUR:SWE:RANG BEST")) return 1;
    //if(ksm_send_command(portdata, ":SOUR:SWE:RANG FIXED")) return 1;
    //vrange = (fabs(vstart)>fabs(vstop))?fabs(vstart):fabs(vstop);
    //sprintf(txt, ":SOUR:VOLT:RANG %lf", vrange);
    //if(ksm_send_command(portdata, txt)) return 1;
  }else{
    if(ksm_send_command(portdata, ":SOUR:SWE:RANG AUTO")) return 1;
    // This sometimes fails - the sourcemeter chooses too low a range for part of the sweep
  }
  if(ksm_send_command(portdata, ":SOUR:SWE:SPAC LIN")) return 1;
  if(ksm_send_command(portdata, ":FORM:ELEM VOLT,CURR")) return 1;
  if(ksm_send_command(portdata, ":SOUR:DEL:AUTO ON")) return 1;

  // need to read back the number of points and program it as the mumber of triggers
  cj_serial_flush_input(portdata);
  if(ksm_send_query(portdata, ":SOUR:SWE:POIN?")) return 1;
  cj_serial_wait_transmit_done(portdata); // wait for request to be sent
  nread = cj_serial_read_to_delimeter(portdata, inpbuf, KSM_INPBUFLEN-1, 2, "\r\n");
  if(nread < 1) return 1; // need at least 1 character, also nread<0 would indicate an error

  if(nread > 0) nread--; // Remove the last character which is hopefully a delimeter
  inpbuf[nread] = 0; // Null terminate reply string

  nconv = sscanf(inpbuf, "%d", &ksm_npoints);
  if(nconv != 1) return 1; // if we didn't convert one value, there is an error
  printf("KSM reported %d points. We wanted %d\n", ksm_npoints, npoints);

  if(speedup & 0x04){
    if(ksm_send_command(portdata, ":SYSTEM:AZERO:STATE OFF")) return 1;
    if(ksm_send_command(portdata, ":SYSTEM:AZERO:STATE ONCE")) return 1;
  }

  if(speedup & 0x08){
    if(ksm_send_command(portdata, ":SENSE:CURR:NPLC 0.01")) return 1;
    if(ksm_send_command(portdata, ":SENSE:VOLT:NPLC 0.01")) return 1;
  }

  if(speedup & 0x10){
    if(ksm_send_command(portdata, ":SENSE:AVER:STAT OFF")) return 1;
  }

  sprintf(txt, ":TRIG:COUN %d", ksm_npoints);
  if(ksm_send_command(portdata, txt)) return 1;

  // Check for errors before turning on the output
  if((err = ksm_check_status(portdata))) return err;

  if(ksm_send_command(portdata, ":OUTP ON")){
    ksm_send_command(portdata, ":OUTP OFF");
    return 1;
  }
  printf("Measuring now...\n");
  cj_serial_flush_input(portdata);
  (void) gettimeofday(meastime, NULL);
  if(ksm_send_query(portdata, ":READ?")){
    ksm_send_command(portdata, ":OUTP OFF");
    return 1;
  }
  cj_serial_wait_transmit_done(portdata); // wait for request to be sent
  for(index=0; index<ksm_npoints; index++){
    for(param=0; param<2; param++){ // step through voltage/current values
      nread = cj_serial_read_to_delimeter(portdata, inpbuf, KSM_INPBUFLEN-1, 3, ",\r\n");
      if(nread < 0){
	ksm_send_command(portdata, ":OUTP OFF");
	return 1; // an error, report it
      }
      if(nread>0){ // At least one character read...
	nread--; // Remove the last character which is hopefully a delimeter
	inpbuf[nread] = 0; // Null terminate reply string
	nconv = sscanf(inpbuf, "%lf", &val);
	if(nconv != 1){
	  ksm_send_command(portdata, ":OUTP OFF");
	  return 1; // if we didn't convert one value, there is an error
	}
	if(index < npoints){ // If we have not run out of space to store the points
	  if(param == 0) v[index] = val;
	  if(param == 1) i[index] = val;
	}
      }else{ // there was no data when we were expecting some
	ksm_send_command(portdata, ":OUTP OFF");
	return 1; // so report an error
      }
    }
    if(index < npoints){
      if((index & 0x3F)==0) printf("%lf\t%lg\n", v[index], i[index]);
    }
  }
  if(ksm_send_command(portdata, ":OUTP OFF")) return 1;

  if(ksm_send_command(portdata, ":TRIG:COUN 1")) return 1;

  if(speedup & 0x04){
    if(ksm_send_command(portdata, ":SYSTEM:AZERO:STATE ON")) return 1;
  }

  if(speedup & 0x08){
    if(ksm_send_command(portdata, ":SENSE:CURR:NPLC 1")) return 1;
    if(ksm_send_command(portdata, ":SENSE:VOLT:NPLC 1")) return 1;
  }

  if(speedup & 0x10){
    if(ksm_send_command(portdata, ":SENSE:AVER:STAT OFF")) return 1; // default OFF.
  }


  if(speedup & 0x04){
    if(ksm_send_command(portdata, ":SYSTEM:AZERO:STATE ON")) return 1;
  }

  //if(ksm_measure_fixedrange_voc(portdata, get_eqhigher_vrange((*voc) * 1.05), voc)) return 1;
  //re - measure the open circuit voltage to be closer in time to the end of the IV curve

  return 0;
}

// From an IV curve, finds the maximum power point and returns the index of that point.
// sign determines the direction of the power flow that is the direction we are looking for.
// If pwrsign = 1, then the point found will have the most positive v * i.
// If the i values are negative, then pwrsign = -1 will probably be wanted
int calc_mpp_index(int npoints, double pwrsign, double *v, double *i){
  int index, mpp_index;
  double mpp;
  double mpp_t;

  mpp_index = 0; // avoids compiler warning about what would happen if all points were <-1e10

  mpp = -1e10; // initialise to very negative value so that one point will certainly be found
  for (index = 0; index < npoints; index++){
    if(pwrsign<0){
      mpp_t = -i[index] * v[index];
    }else{
      mpp_t = i[index] * v[index];
    }
    if (mpp_t > mpp){
      mpp = mpp_t;
      mpp_index = index;
    }
  }
  return mpp_index;
}




// Testing code below

#if 0


#define NPOINTS 101
int main(void){
  const char devname[] = "/dev/ttyUSB-sourcemeter";
  const long int baudrate = 57600;
  //const long int baudrate = 38400;
  //const long int baudrate = 9600;

  const int timeout_10ths = 10;
  cj_serial_t portmem;
  cj_serial_t *portdata;
  int err;
  //  unsigned char inpbuf[KSM_INPBUFLEN];
  //  ssize_t nread;
  //  int i;
  double voc;
  double isc;
  double varray[NPOINTS];
  double iarray[NPOINTS];
  struct timeval meastime;

  portdata = &portmem;

  if((err = ksm_open(portdata, devname, baudrate, timeout_10ths))) return 1;

  err = ksm_reset(portdata);
  printf("ksm_reset() returned %d\n", err);

  err = ksm_check_connection(portdata);
  printf("ksm_check_connection() returned %d\n", err);

  err = ksm_beep_enable(portdata, 0);
  printf("ksm_beep_enable() returned %d\n", err);


  err = ksm_display_enable(portdata, 0);
  printf("ksm_display_enable() returned %d\n", err);

  err = ksm_measure_fixedrange_voc(portdata, 100.0, &voc);
  printf("ksm_measure_fixedrange_voc() returned %d\n", err);
  if(!err) printf("VOC = %lf\n", voc);

  err = ksm_measure_autorange_voc(portdata, &voc);
  printf("ksm_measure_autorange_voc() returned %d\n", err);
  if(!err) printf("VOC = %lf\n", voc);


  err = ksm_measure_fixedrange_isc(portdata, 1.0, &isc);
  printf("ksm_measure_fixedrange_isc() returned %d\n", err);
  if(!err) printf("ISC = %lf\n", isc);

  err = ksm_measure_autorange_isc(portdata, &isc);
  printf("ksm_measure_autorange_isc() returned %d\n", err);
  if(!err) printf("ISC = %lg\n", isc);

  err = ksm_measure_autorange_iv(portdata, NPOINTS, &voc, &isc, varray, iarray, &meastime);
  printf("ksm_measure_autorange_iv() returned %d\n", err);
  if(!err) printf("VOC = %lf, \tISC = %lg\n", voc, isc);

  err = ksm_measure_autorange_iv(portdata, NPOINTS, &voc, &isc, varray, iarray, &meastime);
  printf("ksm_measure_autorange_iv() returned %d\n", err);
  if(!err) printf("VOC = %lf, \tISC = %lg\n", voc, isc);
  ksm_close(portdata);

  int mppi;

  mppi =  calc_mpp_index(NPOINTS, isc, varray, iarray);
  printf("MPPT point %d, V=%lf, I=%lf, P=%lf, FF=%lf\n", mppi, varray[mppi], iarray[mppi], varray[mppi]*iarray[mppi],  varray[mppi]*iarray[mppi]/(voc*isc));


  printf("meastime %ld . %ld \n", (long int)meastime.tv_sec, (long int)meastime.tv_usec);

  return 0;
}

#endif


// Bugs:
// Sourcemeter sometimes returns error code:<-113,"Undefined header">
// If the current increases suddenly during autorange measurement, it will fail
// If the voltage increases suddenly during autorange measurement, it will fail

// Todo:
// Record time at start and end of measurement
// ?Perhaps re-measure VOC after sweep to make it match the curve better
// ?Speed optimisation?
// ?Reduce error handling code?
// Build functions into main program that stores results in database
// Make it resilient to error codes from sourcemeter (auto restart in case of error?)
// Make it resilient to temporary / intermittent failure of USB connection to USB-Serial converter
// ? make it put higher density of points around interesting region of IV curve?



// DONE * Make sure number of points is always as requested
