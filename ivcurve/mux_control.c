// To control relay mux

#include <stdio.h>
#include <time.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <unistd.h>

#include "cj_serial.h"
#include "mux_control.h"

#define MUX_MAX_STRLEN 256

static const struct timespec TRACE_WAIT = {0, 250000000}; // {sec, nsec}
// time to wait from sending serial command till mux relays have switched

// returns 0 on success, non-zero on failure
int mux_set(cj_serial_t *portdata, unsigned int channel){
  int err;
  char s[MUX_MAX_STRLEN];

  if(channel > MUX_ALL_OFF){
    fprintf(stderr, "bad_channel_number");
    return 1;
  }

  cj_serial_flush_input(portdata); // clear input buffer
  sprintf(s, "%c", '0' + MUX_ALL_OFF);
  err = cj_serial_write(portdata, s);
  if(err) return err;
  nanosleep(&TRACE_WAIT, NULL);

  cj_serial_flush_input(portdata); // clear input buffer
  sprintf(s, "%c", '0' + channel);
  err = cj_serial_write(portdata, s);
  if(err) return err;
  nanosleep(&TRACE_WAIT, NULL);

  return 0;
}

// returns 0 on success, non-zero on failure
int mux_open(cj_serial_t *portdata, const char *device_name, long int baudrate, int timeout_10ths){
	int err;

	// Open a serial port
	// timeout is the time in 1/10 s to wait before returning if less data arrives than was supposed to.
	// the timeout is applied between characters, effectively. There may be multiple gaps each less than
	// timeout and it won't return even if the sum of the gaps exceeds timeout.
	// returns -1 on error, or 0 on success
	err = cj_serial_open(portdata, device_name, baudrate, 0, 0, timeout_10ths);
	// 0 is no HW flow control,
	// 1 is enable xon/xoff flow control

	return err;
}


void mux_close(cj_serial_t *portdata){
	if(portdata->f != -1) (void)mux_set(portdata, MUX_ALL_OFF);
	cj_serial_close(portdata);
}

