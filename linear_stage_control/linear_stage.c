// latest version as of 2013_09_06
#include "cj_serial.h"
#include <stdio.h>
#include <stdlib.h>


//const char device_name[] = "/dev/ttyUSB0";
const char device_name[] = "/dev/ttyACM0";
const long int baudrate = 9600;
const int timeout_10ths = 40;

int write_message_with_lf(cj_serial_t *portdata, char *string){
  int err;

//  unsigned char lfstring[] = {'G', '2', '8', 0x0A, 0};
//  unsigned char lfstring[] = {0x0A, 0x00};
  unsigned char lfstring[] = "\n";

  printf("writing <%s>\n", string);

  err = cj_serial_write(portdata, string);
  if(err){
    printf("cj_serial_write() failed\n");
    return err;
  }else{
    printf("cj_serial_write() done\n");
  }

  printf("writing <LF>\n");

  err = cj_serial_write(portdata, lfstring);
  if(err){
    printf("cj_serial_write() failed\n");
    return err;
  }else{
    printf("cj_serial_write() done\n");
  }
  
  printf("calling cj_serial_wait_transmit_done()\n");
  // block until all data previously written has gone out to (or through??) the UART
  cj_serial_wait_transmit_done(portdata);
  printf("done\n");

  return 0;
}

int main(int argc, char *argv[]){
  cj_serial_t portmem;
  cj_serial_t *portdata;
  int err;
  int i;

  portdata = &portmem;


  if(argc < 3){
    printf("Usage: %s <device> <command1> [<command2> ... ]\n where device might be /dev/ttyACM0 and <command> might be \"G28\" or \"G1X100 F1000\"\n", argv[0]);
    exit(1);
  }

  err = cj_serial_open(portdata, argv[1], baudrate, 0, 1, timeout_10ths);
          //  int cj_serial_open(cj_serial_t *portdata, const char *device_name, long int baudrate, int hwfc, int xofc, int timeout);
          // Opens a serial port
          // timeout is the time in 1/10 s to wait before returning if less data arrives than was supposed to.
          // the timeout is applied between characters, effectively. There may be multiple gaps each less than
          // timeout and it won't return even if the sum of the gaps exceeds timeout.
          // returns -1 on error, or 0 on success
          // if hwfc is nonzero then hardware flow control is enabled.
          // if xofc is nonzero then xon / oxff flow control is enabled.

  if (err){
    printf("cj_serial_open() operation failed.\n");
    exit(1);
  }else{
    printf("cj_serial_open() operation ok.\n");
  }


//  printf("delaying (not sure why, but necessary...)\n");
//  sleep(1);
// commented out after reset-on-serial-connect was disabled on the Arduino


//  set_dtr(portdata, 0);


  for(i=2; i<argc; i++){
    write_message_with_lf(portdata, argv[i]);
  }


#if 0
  write_message_with_lf(portdata, argv[2]); //G28
  sleep(5);
  write_message_with_lf(portdata, argv[3]); //G1X50
  set_dtr(portdata, 0);
  sleep(2);
  write_message_with_lf(portdata, argv[4]); //G1X40
  sleep(1);
//  set_dtr(portdata, 1);
  sleep(1);
  write_message_with_lf(portdata, argv[5]); //G1X30
#endif



//  printf("delaying...\n");
//  sleep(4);
//  printf("           done\n");

  cj_serial_close(portdata);
  
  return 0;
}

