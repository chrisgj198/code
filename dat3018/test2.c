// For polling DAT3018 and printing the temperature values
// to compile:
//gcc test.c -lmodbus

#include <modbus/modbus.h>
#include <stdio.h>

const char dat3018_devname[] = "/dev/ttyMODBUS0"; // device for modbus
//const char dat3018_devname[] = "/dev/tty-dat3018"; // device for modbus

const unsigned long int dat3018_baudrate = 38400;


int main(void){
  modbus_t *ctx;
  int rv;
  uint16_t data[256];
  int i;
  FILE *fp;

  printf("About to open %s\n", dat3018_devname);

  ctx = modbus_new_rtu(dat3018_devname, dat3018_baudrate, 'N', 8, 1);
  if (ctx == NULL){
    fprintf(stderr, "modbus_new_rtu() failed\n");
    return -1;
  }



  //modbus_set_debug(ctx, 1);

  rv = modbus_connect(ctx);
  if(rv){
    fprintf(stderr, "modbus_connect() failed\n");
    modbus_free(ctx);
    return -1;
  }

  //modbus_rtu_set_serial_mode(ctx, MODBUS_RTU_RS485);

  //int modbus_set_slave(modbus_t *ctx, int slave);
  rv = modbus_set_slave(ctx, 1);
  if(rv){
    fprintf(stderr, "modbus_set_slave() failed.\n");
    modbus_close(ctx);
    modbus_free(ctx);
    return -1;
  }



  do{
    sleep(1);
    // int modbus_read_input_registers(modbus_t *ctx, int addr, int nb, uint16_t *dest);
    rv = modbus_read_input_registers(ctx, 0, 30, data);
    if(rv != 30){
      fprintf(stderr, "modbus_input_read() failed.\n");
    }else{
      printf("\n\n");
      for(i=0; i<30; i++){
	printf("Reg \t%d\t value \t%d\n", i, data[i]);
      }
      fp = fopen("out.csv", "a");
      if(!fp){
	      printf("failed to open file.\n");
      }else{
	      for(i=13; i<13+8; i++){
		      fprintf(fp, "%d", (int)data[i]);
		      if(i<(13+7)) fprintf(fp, ",");
	      }
	      fprintf(fp, "\n");
	      fclose(fp);
      }
    }
  }while(1);

  //void modbus_close(modbus_t *ctx);
  modbus_close(ctx);

  //void modbus_free(modbus_t *ctx);
  modbus_free(ctx);
}



#if 0

int modbus_set_slave(modbus_t* ctx, int slave);
int modbus_set_error_recovery(modbus_t *ctx, modbus_error_recovery_mode error_recovery);
void modbus_set_socket(modbus_t *ctx, int socket);
int modbus_get_socket(modbus_t *ctx);

void modbus_get_response_timeout(modbus_t *ctx, struct timeval *timeout);
void modbus_set_response_timeout(modbus_t *ctx, const struct timeval *timeout);

void modbus_get_byte_timeout(modbus_t *ctx, struct timeval *timeout);
void modbus_set_byte_timeout(modbus_t *ctx, const struct timeval *timeout);

int modbus_get_header_length(modbus_t *ctx);

int modbus_connect(modbus_t *ctx);
void modbus_close(modbus_t *ctx);

void modbus_free(modbus_t *ctx);

int modbus_flush(modbus_t *ctx);
void modbus_set_debug(modbus_t *ctx, int boolean);

const char *modbus_strerror(int errnum);

int modbus_read_bits(modbus_t *ctx, int addr, int nb, uint8_t *dest);
int modbus_read_input_bits(modbus_t *ctx, int addr, int nb, uint8_t *dest);
int modbus_read_registers(modbus_t *ctx, int addr, int nb, uint16_t *dest);
int modbus_read_input_registers(modbus_t *ctx, int addr, int nb, uint16_t *dest);
int modbus_write_bit(modbus_t *ctx, int coil_addr, int status);
int modbus_write_register(modbus_t *ctx, int reg_addr, int value);
int modbus_write_bits(modbus_t *ctx, int addr, int nb, const uint8_t *data);
int modbus_write_registers(modbus_t *ctx, int addr, int nb, const uint16_t *data);
int modbus_write_and_read_registers(modbus_t *ctx, int write_addr, int write_nb,
                                    const uint16_t *src, int read_addr, int read_nb,
                                    uint16_t *dest);
int modbus_report_slave_id(modbus_t *ctx, uint8_t *dest);

modbus_mapping_t* modbus_mapping_new(int nb_coil_status, int nb_input_status,
                                     int nb_holding_registers, int nb_input_registers);
void modbus_mapping_free(modbus_mapping_t *mb_mapping);

int modbus_send_raw_request(modbus_t *ctx, uint8_t *raw_req, int raw_req_length);

int modbus_receive(modbus_t *ctx, uint8_t *req);
int modbus_receive_from(modbus_t *ctx, int sockfd, uint8_t *req);

int modbus_receive_confirmation(modbus_t *ctx, uint8_t *rsp);

int modbus_reply(modbus_t *ctx, const uint8_t *req,
                 int req_length, modbus_mapping_t *mb_mapping);
int modbus_reply_exception(modbus_t *ctx, const uint8_t *req,
                           unsigned int exception_code);

/**
 * UTILS FUNCTIONS
 **/

#define MODBUS_GET_HIGH_BYTE(data) (((data) >> 8) & 0xFF)
#define MODBUS_GET_LOW_BYTE(data) ((data) & 0xFF)
#define MODBUS_GET_INT32_FROM_INT16(tab_int16, index) ((tab_int16[(index)] << 16) + tab_int16[(index) + 1])
#define MODBUS_GET_INT16_FROM_INT8(tab_int8, index) ((tab_int8[(index)] << 8) + tab_int8[(index) + 1])
#define MODBUS_SET_INT16_TO_INT8(tab_int8, index, value) \
  do { \
  tab_int8[(index)] = (value) >> 8;  \
  tab_int8[(index) + 1] = (value) & 0xFF; \
  } while (0)

void modbus_set_bits_from_byte(uint8_t *dest, int index, const uint8_t value);
void modbus_set_bits_from_bytes(uint8_t *dest, int index, unsigned int nb_bits,
                                const uint8_t *tab_byte);
uint8_t modbus_get_byte_from_bits(const uint8_t *src, int index, unsigned int nb_bits);
float modbus_get_float(const uint16_t *src);
void modbus_set_float(float f, uint16_t *dest);





void modbus_free(modbus_t *ctx);



// from http://code.google.com/p/thp-pj2-08/source/browse/trunk/par_trajectory_planning/src/test.cpp?r=16
#include <modbus/modbus.h>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <cerrno>

int main()
{
  modbus_t* ctx;

  ctx = modbus_new_rtu("/dev/ttyS0", 115200, 'N', 8, 1);
  if (ctx == NULL)
    {
      std::cout << "StepperMotor::init(): unable to create the libmodbus context." << std::endl;
    }
  modbus_set_debug(ctx, 1);
  //struct timeval timeout;
  //timeout.tv_sec = 0;
  //timeout.tv_usec =  10000;
  //modbus_set_timeout_begin(ctx, &timeout);


  if ( modbus_connect(ctx) == -1)
    {
      std::cout << "StepperMotor::init(): connection failed." << std::endl;
    }

  std::cout << "connection success" << std::endl;

  struct timeval timeout_begin_old;
  struct timeval timeout_end_old;

  /* Save original timeout */
  modbus_get_timeout_begin(ctx, &timeout_begin_old);
  printf(" ---> timeout begin tv_sec %ld and timeout tv_usec %ld\n", timeout_begin_old.tv_sec, timeout_begin_old.tv_usec); 

  modbus_get_timeout_end(ctx, &timeout_end_old);
  printf(" ---> timeout end tv_sec %ld and timeout tv_usec %ld\n", timeout_end_old.tv_sec, timeout_end_old.tv_usec);

  modbus_set_slave(ctx, 1);
  printf("errno: %s\n", modbus_strerror(errno));


  // Set M[0]..M[5] to 0 else wrong position is selected 
  int n = modbus_write_register(ctx, 0x001E, 0x0000);
  printf("errno: %s\n", modbus_strerror(errno));

  //  turn ON the motor excitation
  n = modbus_write_register(ctx, 0x001E, 0x2000);
  printf("errno: %s\n", modbus_strerror(errno));


  uint16_t* src = new uint16_t[2];
  // POSITION NO1 is address 0x0402 and 0x0403 and specify position 1000
  src[1] = 0x03E8;
  src[0] = 0x00;
  n = modbus_write_registers(ctx, 0x0402, 2, src);
  printf("errno: %s\n", modbus_strerror(errno));

  // POSITION NO1 set operating speed
  src[1] = 0x1388;
  src[0] = 0x00;
  n = modbus_write_registers(ctx, 0x0502, 2, src);

  // POSITION NO1 set operating mode to link
  n = modbus_write_register(ctx, 0x0701, 0x01);

  // POSITION NO1 set to absolute positioning
  n = modbus_write_register(ctx, 0x0601, 0x01);

  // POSITION NO1 set to sequential mode
  n = modbus_write_register(ctx, 0x0801, 0x01);

  // POSITION NO2 is address 0x0404 and 0x045 and specify position
  src[1] = 0x1710;
  src[0] = 0x00;
  n = modbus_write_registers(ctx, 0x0404, 2, src);

  // POSITION NO2 set operating speed
  src[1] = 0x0800;
  src[0] = 0x00;
  n = modbus_write_registers(ctx, 0x0504, 2, src);

  // POSITION NO2 set operating mode to single
  n = modbus_write_register(ctx, 0x0702, 0x00);

  // POSITION NO2 set to absolute positioning
  n = modbus_write_register(ctx, 0x0602, 0x01);

  // POSITION NO2 set to sequential mode
  n = modbus_write_register(ctx, 0x0802, 0x01);



  // turn start input on
  n = modbus_write_register(ctx, 0x001E, 0x2101);

  // turn start input off
  //n = modbus_write_register(ctx, 0x001E, 0x2000);

  modbus_close(ctx);
  modbus_free(ctx);


  return 0;

}






#endif







