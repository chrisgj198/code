// Chris Jones, ANU
//
// Known bugs:
//   * At least with FTDI USB serial ports, it doesn't seem to be able to flush output
//   * This results in a problem when exiting that it changes back the baud rate to the previous value
//      before the final characters have been transmitted (at the wrong baud rate)
//
// 2013_10_22
// Changes:
// * void cj_serial_close_restore(cj_serial_t *portdata) added
//    which does what void cj_serial_close(cj_serial_t *portdata) used to do
// * void cj_serial_close(cj_serial_t *portdata) now doesn't put the serial settings back how they used to be
// * All baud rates now added
//
// 2014_02_20
// Changes:
// Check whether file descriptor is -1 before trying to close it.
// This means that:
//  -if the port failed to open when an attempt was made to open it,
//   then there is not harm in trying to close it.
//  -if the file descriptor is set to -1 right at the start of the program (using XXXX), then,
//   even if no attempt to open the port is made, then there will still be no harm in trying to close it.



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <termios.h>
#include <time.h>
#include <sys/ioctl.h>

#include "cj_serial.h"



// Uncomment this to print out what is going over the serial comms functions, for debugging
//#define DEBUGPRINT



// See:
// http://www.easysw.com/~mike/serial/serial.html
// http://www.tldp.org/HOWTO/Serial-Programming-HOWTO/x115.html
// http://ftp.sunet.se/pub/Linux/distributions/slackware/slackware-3.3/docs/mini/Serial-Port-Programming
// http://linux.die.net/man/3/cfsetispeed
// http://unixwiz.net/techtips/termios-vmin-vtime.html
// http://www.linux.it/~rubini/docs/serial/serial.html
// http://en.wikibooks.org/wiki/Serial_Programming/termios


#ifdef DEBUGPRINT
void safeprint(char *s){
  while(*s){
    if(*s >= ' '){
      printf("%c", *s);
    }else{
      printf("[0x%02x]", ((int)*s));
    }
    s++;
  }
}
void safenprint(char *s, ssize_t len){
  ssize_t i;
  for(i=0; i<len; i++){
    if(*s >= ' '){
      printf("%c", *s);
    }else{
      printf("[0x%02x]", ((int)*s));
    }
    s++;
  }
}
#endif



// Open a serial port
// timeout is the time in 1/10 s to wait before returning if less data arrives than was supposed to.
// the timeout is applied between characters, effectively. There may be multiple gaps each less than
// timeout and it won't return even if the sum of the gaps exceeds timeout.
// returns -1 on error, or 0 on success
// if hwfc is nonzero then hardware flow control is enabled.
// if xofc is nonzero then xon / oxff flow control is enabled.
int cj_serial_open(cj_serial_t *portdata, const char *device_name, long int baudrate, int hwfc, int xofc, int timeout){
  speed_t speed;
  
	portdata->f = open(device_name, O_RDWR | O_NOCTTY | O_NDELAY);
	// O_RDWR
	// O_NOCTTY flag tells UNIX that this program doesn't want to be the "controlling terminal" for that port
	// O_NODELAY flag tells UNIX that this program doesn't care what state the DCD signal line is in

	if (portdata->f == -1)
		return -1;

	tcgetattr(portdata->f, &(portdata->oldtio)); // make backup of port settings to restore when we quit
	tcgetattr(portdata->f, &(portdata->newtio)); // Get current settings to modify them to suit us.
	// This is a subject of controversy, whether to get the old settings and modify those bits we know about
	// and leave the bits we don't know about as they were before, OR...
	// to freshly define all bits frmo scratch ignoring previous settings of the port.
	// Neither approach is guaranteed to work with a newer OS version or newer other applications:
	// If we leave alone new bits that we don't know about, we may leave them in a state that doesn't work
	// If we set to some fixed value those bits that we don't know about, they may in future have their
	// purpose defined in such a way that whatever we set them to is the setting that doesn't work.


	// We want to be able to wait for serial data to arrive, without the risk of waiting forever
	//  if for some reason the external device never sends the data at all.
	// Therefore, we would like to wait for either a timeout to expire, or a specified amount of data
	// to be received, whichever occurs first.

	fcntl(portdata->f, F_SETFL, 0); // read will block if no bytes present, so timeout can be used
	//fcntl(portdata->f, F_SETFL, FNDELAY); // The FNDELAY option causes the read function to return 0 if no characters are available on the port


	switch(baudrate){
	  case 0:
	    speed = B0;
	    break;
	  case 50:
	    speed = B50;
	    break;
	  case 75:
	    speed = B75;
	    break;
	  case 110:
	    speed = B110;
	    break;
	  case 134:
	    speed = B134;
	    break;
	  case 150:
	    speed = B150;
	    break;
	  case 200:
	    speed = B200;
	    break;
	  case 300:
	    speed = B300;
	    break;
	  case 600:
	    speed = B600;
	    break;
	  case 1200:
	    speed = B1200;
	    break;
	  case 1800:
	    speed = B1800;
	    break;
	  case 2400:
	    speed = B2400;
	    break;
	  case 4800:
	    speed = B4800;
	    break;
	  case 9600:
	    speed = B9600;
	    break;
	  case 19200:
	    speed = B19200;
	    break;
	  case 38400:
	    speed = B38400;
	    break;
	  case 57600:
	    speed = B57600;
	    break;
	  case 115200:
	    speed = B115200;
	    break;
	  case 230400:
	    speed = B230400;
	    break;
	  default:
	    fprintf(stderr, "cj_serial.c : baud rate not yet supported, please add it.\n");
	    return -1;
	}
	cfsetispeed(&(portdata->newtio), speed);
	cfsetospeed(&(portdata->newtio), speed);
	

	portdata->newtio.c_cflag |= CLOCAL;  // Local line - do not change "owner" of port OR?? Ignore modem control lines
	portdata->newtio.c_cflag |= CREAD;   // Enable receiver
	portdata->newtio.c_cflag &= ~CSIZE;  // Clear character size portion
	portdata->newtio.c_cflag |= CS8;     // Now write in new character size 8 bit
	portdata->newtio.c_cflag &= ~PARENB; // No parity
	portdata->newtio.c_cflag &= ~PARODD; // but if we did enable parity it would be even
  portdata->newtio.c_cflag &= ~HUPCL;  // HUPCL = Lower modem control lines after last process closes the device (hang up)
	portdata->newtio.c_cflag &= ~CSTOPB; // No 2 stop bits (so 1 stop bit)
	if(hwfc){
	  portdata->newtio.c_cflag |= CRTSCTS; // Enable hardware flow control
	}else{
	  portdata->newtio.c_cflag &= ~CRTSCTS; // Disable hardware flow control
	}
	//	portdata->newtio.c_cflag &= ~LOBLK; // LOBLK = (not in POSIX) Block output from a noncurrent shell layer. For use by shl (shell layers). (Not implemented on Linux.)
	portdata->newtio.c_cflag &= ~CMSPAR; // CMSPAR = (not in POSIX) Use "stick" (mark/space) parity (supported on certain serial devices): if PARODD is set, the parity bit is always 1; if PARODD is not set, then the parity bit is always 0)

	portdata->newtio.c_lflag &= ~ICANON; // ICANON = Enable canonical input (else raw)
	portdata->newtio.c_lflag &= ~ECHO; // ECHO  = Enable echoing of input characters
	portdata->newtio.c_lflag &= ~ECHOE; // ECHOE  = Echo erase character as BS-SP-BS
	portdata->newtio.c_lflag &= ~ISIG; // ISIG   = Enable SIGINTR, SIGSUSP, SIGDSUSP, and SIGQUIT signals
	portdata->newtio.c_lflag &= ~XCASE;   // XCASE   = Map uppercase - lowercase (obsolete)
	portdata->newtio.c_lflag &= ~ECHOK;   // ECHOK   = Echo NL after kill character
	portdata->newtio.c_lflag &= ~ECHONL;  // ECHONL  = Echo NL
	portdata->newtio.c_lflag &= ~NOFLSH;  // NOFLSH  = Disable flushing of input buffers after interrupt or quit characters
	portdata->newtio.c_lflag &= ~IEXTEN;  // IEXTEN  = Enable extended implementation-defined input processing

	portdata->newtio.c_lflag &= ~ECHOCTL; // ECHOCTL = Echo control characters as ^char and delete as ~?
	portdata->newtio.c_lflag &= ~ECHOPRT; // ECHOPRT = Echo erased character as character erased
	portdata->newtio.c_lflag &= ~ECHOKE;  // ECHOKE  = BS-SP-BS entire line on line kill
	//	portdata->newtio.c_lflag &= ~DEFECHO;  // DEFECHO = (not in POSIX) Echo only when a process is reading. (Not implemented on Linux.)
	portdata->newtio.c_lflag &= ~FLUSHO;  // FLUSHO  = (not in POSIX; not supported under Linux) Output being flushed
	portdata->newtio.c_lflag &= ~PENDIN;  // PENDIN  = (not in POSIX; not supported under Linux) Retype pending input at next read or input char
	portdata->newtio.c_lflag &= ~TOSTOP;  // TOSTOP  = Send SIGTTOU for background output

	if(xofc){
	  portdata->newtio.c_iflag |= IXON;    // IXON    = Enable software flow control (outgoing)
	  portdata->newtio.c_iflag |= IXOFF;   // IXOFF   = Enable software flow control (incoming)
	}else{
	  portdata->newtio.c_iflag &= ~IXON;    // IXON    = Disable software flow control (outgoing)
	  portdata->newtio.c_iflag &= ~IXOFF;   // IXOFF   = Disable software flow control (incoming)
	}
	portdata->newtio.c_iflag &= ~IXANY;   // IXANY   = Allow any character to start flow again
	portdata->newtio.c_iflag &= ~INPCK;	//INPCK  = Enable parity check
	portdata->newtio.c_iflag |=  IGNPAR;	//IGNPAR = Ignore parity errors
	portdata->newtio.c_iflag &= ~PARMRK;	//PARMRK = Mark parity error
	portdata->newtio.c_iflag &= ~ISTRIP;	//ISTRIP = Strip parity (8th) bits
	portdata->newtio.c_iflag |=  IGNBRK;	//IGNBRK = Ignore break condition
	portdata->newtio.c_iflag &= ~BRKINT;	//BRKINT = Send a SIGINT when a break condition is detected
	portdata->newtio.c_iflag &= ~INLCR;	//INLCR  = Map NL to CR
	portdata->newtio.c_iflag &= ~IGNCR;	//IGNCR  = Ignore CR
	portdata->newtio.c_iflag &= ~ICRNL;	//ICRNL  = Map CR to NL
	portdata->newtio.c_iflag &= ~IUCLC;	//IUCLC  = Map uppercase to lowercase
	portdata->newtio.c_iflag &= ~IMAXBEL;	//IMAXBEL = (not in POSIX) Echo BEL on input line too long
	portdata->newtio.c_iflag &= ~IUTF8;	//IUTF8 = (not in POSIX) Input is UTF8; allows character-erase in cooked mode

	portdata->newtio.c_oflag &= ~OPOST; // Raw output

       	portdata->newtio.c_oflag &= ~OLCUC;	// OLCUC (not in POSIX) Map lowercase characters to uppercase on output
	portdata->newtio.c_oflag &= ~ONLCR; // ONLCR (XSI) Map NL to CR-NL on output
	portdata->newtio.c_oflag &= ~OCRNL; // OCRNL = Map CR to NL on output
	portdata->newtio.c_oflag &= ~ONOCR; // ONOCR = Don't output CR at column 0
	portdata->newtio.c_oflag &= ~ONLRET; // ONLRET = Don't output CR
	portdata->newtio.c_oflag &= ~OFILL; // OFILL = Send fill characters for a delay, rather than using a timed delay
	portdata->newtio.c_oflag &= ~OFDEL; // OFDEL = (not in POSIX) Fill character is ASCII DEL (0177). If unset, fill character is ASCII NUL ('\0'). (Not implemented on Linux.)
	portdata->newtio.c_oflag &= ~NLDLY; // Newline delay mask
	portdata->newtio.c_oflag &= ~CRDLY; // Carriage return delay mask
	portdata->newtio.c_oflag &= ~TABDLY; // Horizontal tab delay mask
	portdata->newtio.c_oflag &= ~BSDLY; // Backspace delay mask
	portdata->newtio.c_oflag &= ~VTDLY; // Vertical tab delay mask
	portdata->newtio.c_oflag &= ~FFDLY; // Form feed delay mask

	//portdata->newtio.c_cc[VDISCARD] = ; // 
	//portdata->newtio.c_cc[VDSUSP] = ; // 
	//portdata->newtio.c_cc[VEOF] = ; // 
	//portdata->newtio.c_cc[VEOL] = ; // 
	//portdata->newtio.c_cc[VEOL2] = ; // 
	//portdata->newtio.c_cc[VERASE] = ; // 
	//portdata->newtio.c_cc[VINTR] = ; // 
	//portdata->newtio.c_cc[VKILL] = ; // 
	//portdata->newtio.c_cc[VLNEXT] = ; // 
	//portdata->newtio.c_cc[VQUIT] = ; // 
	//portdata->newtio.c_cc[VREPRINT] = ; // 
	//portdata->newtio.c_cc[VSTART] = ; // 
	//portdata->newtio.c_cc[VSTATUS] = ; // 
	//portdata->newtio.c_cc[VSTOP] = ; // 
	//portdata->newtio.c_cc[VSUSP] = ; // 
	//portdata->newtio.c_cc[VSWTCH] = ; // 
	//portdata->newtio.c_cc[VWERASE] = ; // 

	//portdata->newtio.c_cc[VMIN] = 255; // Return if x characters have been read
	//portdata->newtio.c_cc[VTIME] = 140; // 14 second timeout (max measure time)
	portdata->newtio.c_cc[VMIN] = 0; // See comments about cj_serial_read() below
	portdata->newtio.c_cc[VTIME] = timeout; // See comments about cj_serial_read() below

	tcsetattr(portdata->f, TCSANOW, &(portdata->newtio)); // The TCSANOW constant specifies that all changes should occur immediately

	return 0;
}

// Write out a string, return 1 if this can't be done, 0 if it seems to have worked.
int cj_serial_write(cj_serial_t *portdata, char *string){
  long int n, len;

  #ifdef DEBUGPRINT
  printf("cj_serial_write(port, ");
  safeprint(string);
  printf(")\n");
  fflush(stdout);
  #endif

  //fprintf(stderr, "string is <%s>.\n", string);


  len = strlen(string);
  n = write(portdata->f, string, len);
  if(n != len){
    fprintf(stderr, "cj_serial_write() : Failed to write out string to serial port, write(f, string, %ld) returned %ld\n", len, n);
    //fprintf(stderr, "string was %s.\n", string);
    return 1;
  }
  return 0;
}

// from http://unixwiz.net/techtips/termios-vmin-vtime.html
// VMIN and VTIME defined
// ======================
//
// VMIN is a character count ranging from 0 to 255 characters,
//  and VTIME is time measured in 0.1 second intervals, (0 to 25.5 seconds).
// The value of "zero" is special to both of these parameters, and this
//  suggests four combinations that we'll discuss below.
// In every case, the question is when a read() system call is satisfied,
//  and this is our prototype call:
//
//int n = read(fd, buffer, nbytes);
//
//Keep in mind that the tty driver maintains an input queue of bytes
// already read from the serial line and not passed to the user,
// so not every read() call waits for actual I/O - the read may very well
// be satisfied directly from the input queue.
//
//VMIN = 0 and VTIME = 0
//    This is a completely non-blocking read - the call is satisfied
// immediately directly from the driver's input queue. If data are available,
// it's transferred to the caller's buffer up to nbytes and returned.
// Otherwise zero is immediately returned to indicate "no data". We'll note
// that this is "polling" of the serial port, and it's almost always a bad idea.
// If done repeatedly, it can consume enormous amounts of processor time and is
// highly inefficient. Don't use this mode unless you really,
// really know what you're doing. 
//
//VMIN = 0 and VTIME > 0
//  This is a pure timed read. If data are available in the input queue, it's
// transferred to the caller's buffer up to a maximum of nbytes, and returned
// immediately to the caller. Otherwise the driver blocks until data arrives,
// or when VTIME tenths expire from the start of the call. If the timer
// expires without data, zero is returned. A single byte is sufficient to
// satisfy this read call, but if more is available in the input queue, it's
// returned to the caller. Note that this is an overall timer, not an
// intercharacter one.
//
//VMIN > 0 and VTIME > 0
//    A read() is satisfied when either VMIN characters have been transferred
// to the caller's buffer, or when VTIME tenths expire between characters.
// Since this timer is not started until the first character arrives, this
// call can block indefinitely*(cj) if the serial line is idle. This is the most
// common mode of operation, and we consider VTIME to be an intercharacter
// timeout, not an overall one. This call should never return zero bytes read.
//
//VMIN > 0 and VTIME = 0
//  This is a counted read that is satisfied only when at least VMIN characters
// have been transferred to the caller's buffer - there is no timing component
// involved. This read can be satisfied from the driver's input queue
// (where the call could return immediately), or by waiting for new data to
// arrive: in this respect the call could block indefinitely. We believe that
// it's undefined behavior if nbytes is less then VMIN. 
// ========================================================================
//
//*(cj) above seems to directly contradict:
// http://www.easysw.com/~mike/serial/serial.html#2_5_4
// which says:
// If VMIN is non-zero, VTIME specifies the time to wait for the first
// character read. If a character is read within the time given, any read
// will block (wait) until all VMIN characters are read. That is, once the
// first character is read, the serial interface driver expects to receive
// an entire packet of characters (VMIN bytes total). If no character is
// read within the time allowed, then the call to read returns 0.
// ========================================================================


// Read data until either the number of characters nchar has been read,
// or the overall timeout has expired. If there are more than nchar
// characters in the input buffer, leave any excess characters there
// to be read next time.
//
// Don't NUL terminate the string before returning. Instead return
// the actual number of characters read.
// - maybe we are transferring binary so that NUL has no special meaning.
// The size of the array "data" must be at least nchar chars.
//
// We would like to use the timeout feature of read() rather than needing
// our own calls to sleep(), so we can't use 
//VMIN = 0 and VTIME = 0
//
// We don't want to block forever if there is no input due to severed cable.
// Therefore we can't use:
//VMIN > 0 and VTIME > 0
// nor
//VMIN > 0 and VTIME = 0
//
// Therefore we are left with:
//VMIN = 0 and VTIME > 0
//
// Returns a negative value on error
ssize_t cj_serial_read(cj_serial_t *portdata, char *data, size_t nchar){
  //ssize_t read(int fd, void *buf, size_t count);
  ssize_t nread;
  ssize_t totread;
  char *nextdata;

  #ifdef DEBUGPRINT
  printf("cj_serial_read()");
  fflush(stdout);
  #endif

  //  if(nchar >= SSIZE_MAX) fprintf(stderr, "Error: cj_serial_read():attempting to read more than SSIZE_MAX bytes\n");


  totread = 0;
  nextdata = data;
  do{
    nread = read(portdata->f, nextdata, nchar-totread);
    // This will buffer any characters that were already received or arrive
    //  within the timeout, up to nchar characters.
    // It may return less than nchar characters without waiting for the timeout.
    // If it buffered some characters then it returns the number of characters.
    // If no characters have arrived within the timeout, it returns 0.
    // On error, -1 is returned, and errno is set appropriately.
    if(nread < 0){ // error!
      return nread;
    }
    totread += nread;
    nextdata += nread;
  }while((totread < nchar)&&(nread > 0)); // if nread = 0 (timed out) then it will return

  #ifdef DEBUGPRINT
  printf("read <");
  safenprint(data, totread);
  printf(">\n");
  fflush(stdout);
  #endif

  return totread;
}

// Returns a negative value on error
// Reads up to nchar characters
// Stops if it reads any of the first ndelimeters characters in the array delimeters
// If the inter-character delay exceeds the timeout then it will return also.
// To determine why the function returned:
// If the return value is negative an error occurred reading the serial port, otherwise
// the return value is the number of characters copied to data[]
// If the last character in data[] is a delimeter then that is why this function returned.
// If the return value is equal to nchar then that is why this function returned.
// If none of the above are the case, then a timeout is the reason why it returned.
ssize_t cj_serial_read_to_delimeter(cj_serial_t *portdata, char *data, size_t nchar, unsigned int ndelimeters, char *delimeters){
  //ssize_t read(int fd, void *buf, size_t count);
  ssize_t nread;
  ssize_t totread;
  char *nextdata;
  unsigned char isdelimeter;
  unsigned int i;

  //  if(nchar >= SSIZE_MAX) fprintf(stderr, "Error: cj_serial_read():attempting to read more than SSIZE_MAX bytes\n");

  #ifdef DEBUGPRINT
  printf("cj_serial_read_to_delimeter()");
  fflush(stdout);
  #endif

  isdelimeter = 0;
  totread = 0;
  nextdata = data;
  do{
    nread = read(portdata->f, nextdata, 1);
    // This will read up to one character that was already received or arrives
    //  within the timeout.
    // If it read a character then it returns 1, the number of characters.
    // If no characte./linear_stage /dev/ttyACM0 "G28" "G1X200 F12000"rs have arrived within the timeout, it returns 0.
    // On error, -1 is returned, and errno is set appropriately.
    if(nread < 0){ // error!
      return nread;
    }
    for(i=0; i<ndelimeters; i++){
      if(delimeters[i] == nextdata[0]) isdelimeter = 1;
    }
    totread += nread;
    nextdata += nread;
  }while((totread < nchar)&&(nread > 0)&&(!isdelimeter)); // if nread = 0 (timed out) then it will return

  #ifdef DEBUGPRINT
  printf("read <");
  safenprint(data, totread);
  printf(">\n");
  fflush(stdout);
  #endif

  return totread;
}

// Mark the serial port as closed, so that if we unnecessarily call cj_serial_close(), it won't try to close it again
void cj_serial_mark_port_as_closed(cj_serial_t *portdata){
	portdata->f = -1;
}

// close the serial port and restore the settings to how they were when we opened it
void cj_serial_close_optional_restore(cj_serial_t *portdata, int restore){
	//restore the serial port settings to what they were before this program was running
	struct timespec slp_time;

	// Check whether port was opened...
	if(portdata->f == -1){ // if port was not successfully opened, then
		// don't try to close it or restore settings
		return;
	}

	// Since TCSADRAIN doesn't seem to work, wait for a second, which hopefully will do.
	slp_time.tv_sec = 1;
	slp_time.tv_nsec = 0;
	nanosleep(&slp_time, NULL);

	if(restore) tcsetattr(portdata->f, TCSADRAIN, &(portdata->oldtio));

	// TCSADRAIN means it should wait for the Tx data to be sent before changing attributes.
	// but this might not work on FTDI USB serial ports, as tcdrain() seemingly doesn't work.

	//tcsetattr(portdata->f, TCSANOW, &(portdata->oldtio));
	// The TCSANOW constant specifies that all changes should occur immediately

	// close the port
	close(portdata->f);
	cj_serial_mark_port_as_closed(portdata);
}

// close the serial port and restore the settings to how they were when we opened it
void cj_serial_close_restore(cj_serial_t *portdata){
	cj_serial_close_optional_restore(portdata, 1);
}

// close the serial port without changing the setttings back
void cj_serial_close(cj_serial_t *portdata){
	cj_serial_close_optional_restore(portdata, 0);
}

int get_cts(cj_serial_t *portdata){
  int status;
  ioctl(portdata->f, TIOCMGET, &status);
  return (status & TIOCM_CTS)?1:0;
}
int get_dsr(cj_serial_t *portdata){
  int status;
  ioctl(portdata->f, TIOCMGET, &status);
  return (status & TIOCM_DSR)?1:0;
}
int get_dcd(cj_serial_t *portdata){
  int status;
  ioctl(portdata->f, TIOCMGET, &status);
  return (status & TIOCM_CD)?1:0;
}
int get_ri(cj_serial_t *portdata){
  int status;
  ioctl(portdata->f, TIOCMGET, &status);
  return (status & TIOCM_RI)?1:0;
}
void set_rts(cj_serial_t *portdata, int val){
  int status = TIOCM_RTS;
  ioctl(portdata->f, val?TIOCMBIS:TIOCMBIC, &status);
}
void set_dtr(cj_serial_t *portdata, int val){
  int status = TIOCM_DTR;
  ioctl(portdata->f, val?TIOCMBIS:TIOCMBIC, &status);
}

// Maybe this would work better for flushing?
// ioctl(portdata->f, TCFLSH, 0);

// flush the read buffer
void cj_serial_flush_input(cj_serial_t *portdata){
  tcflush(portdata->f, TCIFLUSH); // Clear input buffer
}

// flush the output buffer
void cj_serial_flush_output(cj_serial_t *portdata){
  tcflush(portdata->f, TCOFLUSH); // Clear output buffer
}


// block until all data previously written has gone out to (or through??) the UART
// Seems not to work on FTDI
void cj_serial_wait_transmit_done(cj_serial_t *portdata){
  tcdrain(portdata->f);
}
