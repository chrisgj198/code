//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <fcntl.h>
//#include <errno.h>
#include <unistd.h>
#include <termios.h>

typedef struct {
  int f; // will hold value returned by open()
  struct termios oldtio; // holds options for the port before it was opened by this program, to restore later
  struct termios newtio; // holds options for the port that we are using
} cj_serial_t;


// Opens a serial port
// timeout is the time in 1/10 s to wait before returning if less data arrives than was supposed to.
// the timeout is applied between characters, effectively. There may be multiple gaps each less than
// timeout and it won't return even if the sum of the gaps exceeds timeout.
// returns -1 on error, or 0 on success
// if hwfc is nonzero then hardware flow control is enabled.
// if xofc is nonzero then xon / oxff flow control is enabled.
int cj_serial_open(cj_serial_t *portdata, const char *device_name, long int baudrate, int hwfc, int xofc, int timeout);


// Write out a string, return 1 if this can't be done, 0 if it seems to have worked.
int cj_serial_write(cj_serial_t *portdata, char *string);

// Read data until either the number of characters nchar has been read,
// or the overall timeout has expired. If there are more than nchar
// characters in the input buffer, leave any excess characters there
// to be read next time.
//
// Don't NUL terminate the string before returning. Instead return
// the actual number of characters read.
// - maybe we are transferring binary so that NUL has no special meaning.
// The size of the array "data" must be at least nchar chars.
//
// Returns a negative value on error.
ssize_t cj_serial_read(cj_serial_t *portdata, char *data, size_t nchar);

// Returns a negative value on error
// Reads up to nchar characters
// Stops if it reads any of the first ndelimeters characters in the array delimeters
// If the inter-character delay exceeds the timeout then it will return also.
// To determine why the function returned:
// If the return value is negative an error occurred reading the serial port, otherwise
// the return value is the number of characters copied to data[]
// If the last character in data[] is a delimeter then that is why this function returned.
// If the return value is equal to nchar then that is why this function returned.
// If none of the above are the case, then a timeout is the reason why it returned.
ssize_t cj_serial_read_to_delimeter(cj_serial_t *portdata, char *data, size_t nchar, unsigned int ndelimeters, char *delimeters);

// Mark the serial port as closed, so that if we unnecessarily call cj_serial_close(), it won't try to close it again
void cj_serial_mark_port_as_closed(cj_serial_t *portdata);

// close the serial port
//restore the serial port settings to what they were before this program was running
void cj_serial_close_restore(cj_serial_t *portdata);

// close the serial port
//don't restore the serial port settings to what they were before this program was running
void cj_serial_close(cj_serial_t *portdata);

int get_cts(cj_serial_t *portdata);
int get_dsr(cj_serial_t *portdata);
int get_dcd(cj_serial_t *portdata);
int get_ri(cj_serial_t *portdata);
void set_rts(cj_serial_t *portdata, int val);
void set_dtr(cj_serial_t *portdata, int val);

// flush the read buffer
// Discards anything that has been received but not processed.
void cj_serial_flush_input(cj_serial_t *portdata);


// flush the output buffer
// Discards anything that has been written and is in the buffer waiting to be sent
void cj_serial_flush_output(cj_serial_t *portdata);


// block until all data previously written has gone out to (or through??) the UART
void cj_serial_wait_transmit_done(cj_serial_t *portdata);

