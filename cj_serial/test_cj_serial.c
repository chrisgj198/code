// latest version as of 2013_09_06
#include "cj_serial.h"


#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <fcntl.h>
//#include <errno.h>
//#include <unistd.h>
//#include <termios.h>


//const char device_name[] = "/dev/ttyUSB0";
const char device_name[] = "/dev/ttyACM0";
const long int baudrate = 9600;
const int timeout_10ths = 40;
#define INPBUFLEN 5

int main(void){
  cj_serial_t portmem;
  cj_serial_t *portdata;
  int err;
  int quit=0;
  unsigned char inpbuf[INPBUFLEN];
  ssize_t nread;
  int i;
  unsigned char message[] = {'G', '2', '8', 0x0A, 0};

  char cmd[256];

  portdata = &portmem;

  while(!quit){
    printf("Enter an option (O=Open, F=Flush output, I=Flush input, C=Close, R=send_CR, L=send_LF, M=send G28+LF, Q=quit) then press enter...\n");
    scanf("%80s", cmd);
    switch(cmd[0]){
      case 'O':
        err = cj_serial_open(portdata, device_name, baudrate, 0, 1, timeout_10ths);
          //  int cj_serial_open(cj_serial_t *portdata, const char *device_name, long int baudrate, int hwfc, int xofc, int timeout);
          // Opens a serial port
          // timeout is the time in 1/10 s to wait before returning if less data arrives than was supposed to.
          // the timeout is applied between characters, effectively. There may be multiple gaps each less than
          // timeout and it won't return even if the sum of the gaps exceeds timeout.
          // returns -1 on error, or 0 on success
          // if hwfc is nonzero then hardware flow control is enabled.
          // if xofc is nonzero then xon / oxff flow control is enabled.

        if (err) printf("cj_serial_open() operation failed.\n");
        break;

      case 'F':
        printf("flushing output\n");
        cj_serial_flush_output(portdata);
        printf("done\n");
        break;

      case 'I':
        printf("flushing input\n");
        cj_serial_flush_input(portdata);
        printf("done\n");
        break;

      case 'M':
        err = cj_serial_write(portdata, message);
        if(err){
          printf("cj_serial_write() failed\n");
        }else{
          printf("cj_serial_write() done\n");
        }
        printf("calling cj_serial_wait_transmit_done()\n");
        // block until all data previously written has gone out to (or through??) the UART
        cj_serial_wait_transmit_done(portdata);
        printf("done\n");
        break;

      case 'C':
        cj_serial_close(portdata);
        break;
        
      case 'Q':
        quit=1;
        break;
    }
  }    
}

